

ReNN : A machine learning library for JS developers using [
ReScript](https://rescript-lang.org/)


The library is written in ReScript and compiled to JS for each file.

ReScript supports ML, and its own ReScript syntax, the majority of the library is 
written in OCaml syntax, you can see [utils.ml](src/utils.ml) and check its generated 
output [utils.js](src/utils.js)
## Build

```
npm install
npx rescript
```
npm is the JavaScript's package manager, it comes with [node](https://nodejs.org/en/), when 
node get installed, both the npm and npx commands comes with it.

The code uses the latest feature SIMD which is not enabled by default yet.
To run the code in the browser, you have to pass --enable-features=WebAssemblySimd.
To run the code in NodeJS, you have to pass `--experimental-wasm-simd` to node

## Test

Run 
```
npx mocha src/test/*test.js
```

## Directory layout

```
src # code where the main library lives
├── test # test code
├── unused # experimental
├── wasm # wasm acceleration
└── webgl # webgl exeriment
```


