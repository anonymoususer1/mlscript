
let suites,id  = ref [], ref 0
let {from_pair_suites; eq_suites} = (module Mt)
module P = Node.Path
let eq a b = eq_suites ~test_id:id ~suites a b 

let p = match [%node __dirname] with Some x -> x | None -> assert false
let label_tensor = UI8_IO.loadMnist (P.join [|p; ".."; "..";"dataset"; "train-labels-idx1-ubyte"|])
let image_tensor = 
  UI8_IO.loadMnistImage  (P.join [|p; ".."; "..";"dataset"; "train-images-idx3-ubyte"|])
  |. UI8.reshape [|60_000; 28;28|]

let label_one_shot = 
  label_tensor 
  |. UI8.one_shot 10  
let test loc  i =
  let u = 
      label_one_shot    
    |. UI8.slice [|i|] in 
    eq loc (UI8.get u [|UI8.get label_tensor [|i |] |] ) 1 

;; test __LOC__ 77 
;; test __LOC__ 22  
;; eq __LOC__   
  ( UI8.subarr label_tensor 0 10 |. UI8.toArray )
  [|5; 0; 4; 1; 9; 2; 1; 3; 1; 4|]
let sample = UI8.from [|3;2;2;3|]

let f64 = F64.from2D [|
    [|3.;2.;1.;4.|];
    [|3.;2.;1.;4.|];
    [|3.;2.;1.;4.|];
    [|3.;2.;1.;4.|];
  |]

let h = f64  |. F64.sampleUI8 sample |. F64.toArray

;; eq __LOC__ h [|4.;1.;1.;4.|]
;; eq __LOC__ 
  (image_tensor |. UI8.slice [|0; 7|]  |. UI8.toArray
  ) [|0;0;0;0;0;0;0;49;238;253;253;253;253;253;253;253;253;251;93;82;82;56;39;0;0;0;0;0|]

;; eq __LOC__
  (image_tensor |. UI8.slice [|33; 19|]  |. UI8.toArray
  )
  [|0;0;0;0;0;0;0;0;0;0;0;25;182;253;203;0;0;0;0;0;0;0;0;0;0;0;0;0|]

(* test(233,17) *)
;; eq __LOC__ 
  (image_tensor |. UI8.slice [|233;17|] |. UI8.toArray)
  [|0;0;0;0;0;197;252;252;190;12;0;29;252;253;252;252;252;252;140;0;0;0;0;0;0;0;0;0|]

;; from_pair_suites __FILE__ !suites