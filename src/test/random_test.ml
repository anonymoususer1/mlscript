let suites,id  = ref [], ref 0
let {from_pair_suites; eq_suites; bool_suites} = (module Mt)

let eq a b = eq_suites ~test_id:id ~suites a b 
let b a = bool_suites ~test_id:id ~suites a

;; eq __LOC__ (RandomUtil.randint 3 4) 3 

let h () = 
  (* Random.self_init (); *)
  let s = ref 0. in   
  for _ = 0 to  9999 do 
    s := !s  +. RandomUtil.randn ()  
  done;
  !s /. 10000.


;; Js.log (h())      
;; Js.log (RandomUtil.randint 3 15)
;; b __LOC__ (abs_float (h()) < 0.1)
;; Js.log (F64.randn ([|3;4|]))
;; from_pair_suites __FILE__ !suites