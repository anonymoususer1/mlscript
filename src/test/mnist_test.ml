
[@@@warning"-44"]
let suites,id  = ref [], ref 0
let {from_pair_suites; eq_suites; bool_suites} = (module Mt)
let b a  b = bool_suites ~test_id:id ~suites a b 

let p = 
  match [%node __dirname] with  
  | Some x -> x 
  | None -> assert false
module P = Node.Path
let label_tensor : F64.t =  
  UI8_IO.loadMnist (P.join [|p;  ".."; "..";"dataset"; "train-labels-idx1-ubyte"|])
  |. UI8.one_shot 10 
  |. F64.ofUI8

let train_tensor : F64.t  =
  UI8_IO.loadMnistImage  (P.join [|p; ".."; ".."; "dataset"; "train-images-idx3-ubyte"|])
  |. UI8.reshape [|60_000; 784|]
  |. F64.ofUI8
  |. F64.map (fun [@bs] x -> x /. 255.)
;;

let ((w1 : float array array), (w2 : float array array)) = [%raw{|function(){
  let {W1,W2} = require(`${__dirname}/../../dataset/params.json`)
  return [W1,W2] 
}|}]()

let w1 = F64.from2D w1 |. F64.reshape [|784;50|]
let w2 = F64.from2D w2 |. F64.reshape [|50;10|]

let weight_init_std = 0.01
(* let w1 = 
  F64.(randn [|784; 50|] |. map (fun [@bs] x ->  weight_init_std *. x )) *)
let w1' = F64.zeros w1.shape 
let b1 = 
  F64.zeros [|50|]
let b1' = F64.zeros b1.shape
(* let w2 = 
  F64.(randn [|50; 10|] |. map (fun [@bs] x -> weight_init_std *. x )) *)
let w2' = F64.zeros w2.shape
let b2 = 
  F64.zeros [|10|]    
let b2' = F64.zeros b2.shape   
;;

let batch_size = 100. 
let predict (input : F64.t) (t : F64.t)  : float =
  let open F64 in  
  let z1 = (input  @ w1) + b1 in 
  let y1 = sigmoid z1 in 
  let z2 = (y1 @ w2) + b2 in
  let y = softmax z2 in   
  let loss =  
    (- 1. /. batch_size) *. sum_all (t * log y )  in 
  (* Js.log loss; *)
  loss




let train (train_tensor : F64.t) (label_tensor : F64.t)  = 
  let losses = [||] in 
  for i = 0 to 4 do 
    let input = (F64.subarr train_tensor (i * 100)  100) in 
    let t =  (F64.subarr label_tensor (i * 100) 100) in  
    let open F64 in  
    let z1 = (input @ w1) + b1 in 
    let y1 = sigmoid z1 in 
    let z2 = (y1 @ w2 ) + b2 in
    let y = softmax z2 in   
    let loss = (- 1. /. batch_size) *. sum_all (t * log y ) in 
    Js.Array2.push losses loss |. ignore ; 
    let z2' = (y - t) |. map_ (fun [@bs] x ->  x /. batch_size)  in 
    let w2' = transpose y1 @ z2' in 
    let b2' = sum z2' ~axis:0 ~keepDims:false in     
    let y1' = z2' @ transpose w2 in 
    let z1' = y1' * ( y1 |. map (fun [@bs] x-> (1.-. x) *. x ) ) in 
    let w1' = transpose input @ z1' in 
    let b1' =  sum z1' ~axis:0 ~keepDims:false in 
    (* assert (F64.is_close b1' b1_num); *)
    (* [|w1;w2;b1;b2|] |. Belt.Array.forEachU (fun [@bs] x -> assert (F64.no_nan x)); *)
    update_with_derivative w1 w1'; 
    update_with_derivative w2 w2';
    update_with_derivative b1 b1';
    update_with_derivative b2 b2';
  done ;
  losses

;;


let batch_size = 100
let learning_rate = 0.1
let train_size = train_tensor.shape.(0)

;; let r = train train_tensor label_tensor

;; b __LOC__ (F64.(is_close (from r) (from [|2.4182714462632364; 2.3364919952214307; 2.349964392903516; 2.299976102309812; 2.304668423087476|])))
(* [] *)
;; from_pair_suites __FILE__ !suites