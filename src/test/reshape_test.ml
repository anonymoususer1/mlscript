
let suites,id  = ref [], ref 0
let {from_pair_suites; eq_suites} = (module Mt)
let {arange; view;swap; reshape} = (module Ndarray)
let eq a b = eq_suites ~test_id:id ~suites a b 

let u2 = arange 0 24 |. view [|3;2;1;1;4;1|]

;; eq __LOC__ u2.stride [|8;4;4;4;1;1|]

let u3 = arange 0 24 |. view [| 4 ; 6|]
let u4 = u3 |. swap 0 1 
;; Js.log u4 

let u5  = u4 |. reshape [|2;12|]
;; Js.log u5

;; eq __LOC__ u5.stride [|12;1|] (*re-alloc*)
(* let is_a_shuffle (x : float array) (y : float array) =  *)
  
(* ;; Belt.Array.sort *)

;; from_pair_suites __FILE__ !suites