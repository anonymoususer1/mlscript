
let suites,id  = ref [], ref 0
let {from_pair_suites; eq_suites; bool_suites} = (module Mt)

let eq a b = eq_suites ~test_id:id ~suites a b 
let b a = bool_suites ~test_id:id ~suites a
let {from; arange;from2D; toArray = toFloatArray } = (module F64)

let testData = from2D [|
    [|2.;4.; 1.|];
    [|4.;1.; 1. |];
    [|2.;2.; 1. |];
  |]

let out = F64.square_sum testData


;; b  __LOC__
  (F64.is_close out  (from [|21.;18.;9.|]))

(* let derive = Grad.numeric_gradient (fun x -> F64.square_sum  x ) testData
let symbol = F64.map testData (fun [@bs] x -> 2. *. x )


;; b __LOC__ 
  (F64.is_close ~diff:1e-3
    derive symbol)  *)
  

;; Mt.from_pair_suites __FILE__ !suites