// Generated by ReScript, PLEASE EDIT WITH CARE
'use strict';

var Mt = require("./mt.js");
var Ndarray = require("../ndarray.js");

var suites = {
  contents: /* [] */0
};

var id = {
  contents: 0
};

function eq(a, b) {
  return function (param) {
    return Mt.eq_suites(id, suites, a, b, param);
  };
}

var u2 = Ndarray.view(Ndarray.arange(0, 24), [
      3,
      2,
      1,
      1,
      4,
      1
    ]);

eq("File \"reshape_test.ml\", line 9, characters 6-13", u2.stride)([
      8,
      4,
      4,
      4,
      1,
      1
    ]);

var u3 = Ndarray.view(Ndarray.arange(0, 24), [
      4,
      6
    ]);

var u4 = Ndarray.swap(u3, 0, 1);

console.log(u4);

var u5 = Ndarray.reshape(u4, [
      2,
      12
    ]);

console.log(u5);

eq("File \"reshape_test.ml\", line 18, characters 6-13", u5.stride)([
      12,
      1
    ]);

Mt.from_pair_suites("reshape_test.ml", suites.contents);

var from_pair_suites = Mt.from_pair_suites;

var eq_suites = Mt.eq_suites;

var arange = Ndarray.arange;

var view = Ndarray.view;

var swap = Ndarray.swap;

var reshape = Ndarray.reshape;

exports.suites = suites;
exports.id = id;
exports.from_pair_suites = from_pair_suites;
exports.eq_suites = eq_suites;
exports.arange = arange;
exports.view = view;
exports.swap = swap;
exports.reshape = reshape;
exports.eq = eq;
exports.u2 = u2;
exports.u3 = u3;
exports.u4 = u4;
exports.u5 = u5;
/* u2 Not a pure module */
