[@@@warning "-44"]
let suites,id  = ref [], ref 0
let {from_pair_suites; eq_suites; bool_suites} = (module Mt)

let eq a b = eq_suites ~test_id:id ~suites a b 
let b a  b = bool_suites ~test_id:id ~suites a b 
let {from; arange;from2D; toArray = toFloatArray } = (module F64)
module T= F64

let original = arange 1 10 

let o3 = 
    arange 0 6 
    |. F64.reshape [|2;3|]
    |. F64.transpose
    |. F64.toArray

;; eq __LOC__ o3 [|0.; 3.; 1.;4.;2.;5.|]    


let o2 = original
         |. F64.reshape [|3;3|]
let u = 
  o2
  |. F64.subarr 1 2 


let v = arange 4 10  
(* ;; Js.log2 u v  *)
;; b __LOC__ (F64.neq u v)

;; b __LOC__ (F64.eq u (v |. F64.(reshape (shape u))))
let speicalv  =  1234.  ;;


;; u |. F64.set [|0 ;0|] speicalv

(* ;; Js.log2 original u *)
;; eq __LOC__ (F64.get u [|0;0|]) speicalv
;; eq __LOC__ (F64.get original [|3|] ) speicalv
;; eq __LOC__ (F64.get o2 [|1;0|]) speicalv

let h = F64.copy u 
;; h |. F64.map_ (fun[@bs] x -> x +. 1.) |. ignore ;;
;; eq __LOC__ (F64.get u [|0;0|] +. 1.) (F64.get h [|0;0|])
;; Js.log h 

let z = (F64.max h)|.F64.toArray 
;; eq __LOC__ z [|1235.;10.|] ;;


let h = from2D [|[| 3.; 1.|]; [|2.;0.5|]|]

;; eq __LOC__ 
  (F64.max h |. toFloatArray )
  ([|3.;2.|])

;;   eq __LOC__ 
  (F64.max h ~axis:0 |. toFloatArray )
  ([|3.;1.|])

(* ;; let hh = from3D    *)
let h0 = T.arange 0 100 
         |. T.reshape  [|10;10|]
         |. T.shuffle_dim0
         |. T.max
         |. T.toArray
let h = h0  
        |. Belt.SortArray.stableSortBy Pervasives.compare

let data3d = 
  T.from3D 

    ([|[|[|8.; 3.; 9.|];
         [|5.; 0.; 9.|]|];

       [|[|7.; 1.; 4.|];
         [|3.; 9.; 1.|]|];

       [|[|8.; 0.; 0.|];
         [|0.; 6.; 7.|]|]|])

(* 3,2,3
   1 * 6 + 0 * 2 
   1 * 6 + 1 * 3  
*)         
let view ind = 
  data3d 
  |. T.slice ind
  |. T.toArray

;; let h1 = data3d |. T.slice [|1;0|] |. T.toArray
;; let h2 = data3d |. T.slice [|1;1|] |. T.toArray


;; eq __LOC__ (view [|1;0|]) [|7.;1.;4.|]
;; eq __LOC__ (view [|1;1|]) [|3.;9.;1.|]
;; eq __LOC__ (view [|2;1|]) [|0.;6.;7.|]


;; (* (3,2,3) *)
eq __LOC__
  (data3d 
   |. T.max
   |. T.toIntArray )
  [|9;9;7;9;8;7|]

;;    eq __LOC__
  (data3d 
   |. T.max ~axis:0
   |. T.toIntArray )
  [|8;3;9;5;9;9|]

;; eq __LOC__ 
  (data3d
   |. T.max ~axis:1
   |. T.toIntArray)
  [|8;3;9;7;9;4;8;6;7|]

;; eq __LOC__ h ([|9.;19.;29.;39.;49.;59.;69.;79.;89.;99.|])  

let randomTestData = [23; 87; 50; 85;  4; 31;  4; 43; 76; 26; 13; 30; 25; 53; 74; 69; 85;
                      27; 45; 63; 75; 30;  7; 50; 88; 94; 62; 22;  6; 30;  2; 12; 35; 15;
                      98; 43; 55; 58; 35; 82; 12; 48; 97; 79; 65;  5; 67; 94; 48; 81;  1;
                      59; 19; 70; 85; 95; 57; 52; 81; 20; 26; 51; 75;  0; 77; 33; 68; 72;
                      55; 92; 36; 54; 46; 42; 25; 57; 69; 13; 92;  3; 77; 42; 76; 77;  6;
                      39; 63;  7; 75; 71; 28; 30; 99; 13; 15; 74; 91;  0; 90; 82; 14; 76;
                      24; 62; 11;  0; 12; 86; 62; 77; 53; 35; 31; 48; 87; 36;  7; 85; 69;
                      89]
let tensor = 
  T.from (Array.map (fun x -> float x)(Array.of_list randomTestData))
  |. T.reshape [|4;3;2;5|]

let max tensor dim = 
  tensor |. T.max ~axis:dim   |. T.toIntArray

;; eq __LOC__ (max tensor 0)   
  [|28;87;99;85;98;74;91;72;90;92;36;76;97;79;74;69;85;94;92;81;77;59;76;77;88;95;63;85;81;89|]
;; eq __LOC__ (max tensor 1)
  [|75;87;50;85;88;94;85;43;76;63;12;59;97;79;98;95;67;94;81;82;77;54;76;77;77;57;69;72;92;92;53;76;99;62;87;74;91;86;90;89|]
;; eq __LOC__ (max tensor 2)
  [|31;87;50;85;26;69;85;27;53;74;94;62;22;50;88;43;55;58;35;98;12;67;97;79;81;95;59;52;81;85;33;68;75;55;92;57;69;46;92;25;77;63;76;77;71;74;91;99;90;82;14;76;86;62;77;53;35;85;69;89|]
;; eq __LOC__ (max tensor 3)  
  [|87;76;74;85;88;94;98;82;97;94;85;95;77;92;54;92;77;75;99;91;76;86;87;89|]
let argmax tensor i = 
  F64.argmax tensor ~axis:i |. I32.toArray
;; eq __LOC__ 
  (argmax tensor 0)
  [|3;0;3;0;1;3;3;2;3;2;2;3;1;1;0;0;0;1;2;1;2;1;2;2;0;1;2;3;1;3|]
;; eq __LOC__
  (argmax tensor 1)
  [|2;0;0;0;2;2;1;0;0;1;1;2;1;1;0;2;1;1;2;0;2;1;2;2;0;1;1;0;1;0;2;1;0;1;2;0;0;1;0;2|];;
;; eq __LOC__  
  (argmax tensor 2)
  [|1;0;0;0;1;1;1;1;0;0;1;1;1;0;0;1;1;1;1;0;0;1;0;0;1;1;0;1;1;0;1;1;0;1;1;1;1;0;1;0;0;1;0;0;1;1;1;0;1;1;0;0;1;0;1;0;0;1;1;1|]
;; eq __LOC__
  (argmax tensor 3)  
  [|1;3;4;1;4;0;4;4;2;2;4;0;4;4;1;3;0;3;2;1;1;2;4;4|]

let test (a : F64.t) (b : F64.t) op = 
  let c = op a b in   
  F64.toArray c 

let ta = from2D [|
    [|3.;5.|];
    [|2.;7.|]
  |]
let tb = from 
    [|1.;4.|]
let tc = from2D [|
    [|5.;5.|];
    [|6.;6.|]
  |]

let te = from [|1.; 4.|] |. F64.reshape [|2;1|]
let td = tb |. F64.reshape [|1;2|]
;; eq __LOC__ (test ta tb F64.sub) [|2.;1.;1.;3.|]
;; eq __LOC__ (test tb ta F64.sub) [|-2.;-1.;-1.;-3.|]
;; eq __LOC__ (test ta tc F64.sub) [|-2.;0.;-4.;1.|]
;; eq __LOC__ (test ta td F64.sub) [|2.;1.;1.;3.|]
;; eq __LOC__ (test ta te F64.sub) [|2.;4.;-2.;3.|]
;; eq __LOC__ (test te ta F64.sub) [|-2.;-4.;2.;-3.|]
;; b __LOC__ (not (Belt.SortArray.isSorted h0 Pervasives.compare))

let softmax_a = F64.(softmax (from [|1.;2.;3.;4.|])) 


;; 
b __LOC__ 
  (F64.(is_close softmax_a
          (from [|0.0320586 ; 0.08714432; 0.23688282; 0.64391426|])))
;; b __LOC__ 
  F64.(is_close (softmax (from2D [|[|1.;2.;3.;4.|]; [|2.;3.;5.;7.|]|]))
         (from2D [|[|0.0320586 ; 0.08714432; 0.23688282; 0.64391426|];
                   [|0.00580663; 0.01578405; 0.11662925; 0.86178007|]|]) 
      )

;; eq __LOC__ 
  (test (from2D [|[|1.;2.;3.|];[|4.;5.;6.|]|])
     (from2D [|[|2.|];[|4.|]; [|5.|] |] )
     F64.dot    
  )
  [|25.; 58.|]      

;; eq __LOC__ 
  (test (from2D [|
       [|1.;2.;3.|];
       [|4.;5.;6.|];
       [|7.;8.;9.|]
     |])
      (from2D [|[|2.|];[|4.|]; [|5.|] |] )
      F64.dot    
  )
  [|25.; 58.; 91.|]        

;;  eq __LOC__ 
  (test (from2D [|
       [|1.;2.;3.|];
       [|4.;5.;6.|];
       [|7.;8.;9.|]
     |])
      (from2D [|[|2.;2.|];[|4.;1.|]; [|5.;2.|] |] )
      F64.dot    
  )
  [|25.;10. ;58.; 25.; 91.;40.|]        

;;  eq __LOC__ 
  (test (from2D [|
       [|1.;2.;3.|];
       [|4.;5.;6.|];
     |])
      (from2D [|[|2.;2.|];[|4.;1.|]; [|5.;2.|] |] )
      F64.dot    
  )
  [|25.;10. ;58.; 25.;|]     

;;  eq __LOC__ 
  (test (from2D [|
       [|1.;2.;3.|];
       [|4.;5.;6.|];
     |])
      (from [|1.;2.;3.|])
      F64.dot    
  )
  [|14.;32.|]     

;;  eq __LOC__ 
  (test (from2D [|
       [|1.;2.;3.;8.|];
       [|4.;5.;6.;9.|];
     |])
      (from [|1.;2.;3.;3.|])
      F64.dot    
  )
  [|38.;59.|]     


let loss_forward x t = 
  let y = F64.softmax x  in 
  F64.cross_entropy_error y t 


let loss =   (loss_forward (F64.from2D [|
    [|1.;2.;3.|];
    [|2.;3.;4.|]
  |]) (UI8.from [|1;2|] )  )

;; b __LOC__ (abs_float (loss -. 0.9076) < 1e-4)   
;; from_pair_suites __FILE__ !suites