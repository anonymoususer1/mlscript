[@@@warning "-44"]
let suites,id  = ref [], ref 0

let {from_pair_suites; eq_suites; bool_suites} = (module Mt)

module S = TensorShape
let eq a b = eq_suites ~test_id:id ~suites a b 
let b a = bool_suites ~test_id:id ~suites a


;; eq __LOC__
  (S.to_dimension [|2;3|] 4) [|1;1;2;3|]

;; eq __LOC__
  (S.to_dimension [|2;3|] 1) [|6|]  
;; eq __LOC__
  (S.to_dimension [|2;3|] 2) [|2;3|]    
(**
   shall we optimize cases like ([|1;2;3|], [|2;3|])
*)

let c (shape1 : int list) (shape2 : int list) = 
  let a = Array.of_list shape1 in 
  let b = Array.of_list shape2  in 
  TensorShape.cat a b 

;; eq __LOC__  (c [1;1;2;3] [1;2;2;3]) (`b_has_a_suffix 1)

;; eq __LOC__ (c [2;1;1;1] [2;2;1;1]) (`b_has_a_prefix 1)
;; eq __LOC__ (c [1;2;2;3] [1;1;2;3]) (`a_has_b_suffix 1)
;; eq __LOC__ (c [2;2;1;1] [2;1;1;1]) (`a_has_b_prefix 1)
;; eq __LOC__ 
  (c [1;1;1;1;2;3] [1;1;2;2;2;3])
  (`b_has_a_suffix 3) 

;; eq __LOC__ 
  (c [2;2;1;1;2;3] [2;2;2;2;1;1])
  (`mismatch)    
;;   eq __LOC__ 
  (c [2;2;1;1;1;1] [2;2;2;1;2;3])
  (`b_has_a_prefix 2)    


#if 0 then
;; eq __LOC__ 
  (S.classify [|1;2;3|] [|2;3|])
  `first_contains_second

(* ;; eq __LOC__ 
   (S.classify [|2;2;3|] [|1;2;3|])
   `first_contains_second   *)
;; eq __LOC__ 
  (S.classify [|2;1;2;3|] [|2;3|])
  `first_contains_second

;; eq __LOC__
  (S.classify [|1;2;1;3|] [|2;3|])
  `unknown

;; eq __LOC__ (S.classify [|3;2;3|] [|2;3|])
  `first_contains_second

;; eq __LOC__ (S.classify [|2;3|] [|2;3|])
  `same 
;;  eq __LOC__ (S.classify [||] [||])
  `same 
;;  eq __LOC__ (S.classify [|2;3|] [|3;2;3|])
  `second_contains_first

;; eq __LOC__ (S.classify [|3;4;7|] [|7;5|])  
  `unknown
#end  
;; from_pair_suites __FILE__ !suites