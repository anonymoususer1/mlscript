


type t = int array
(* Make sure this is immutable array  *)
(* TODO: should we allow empty array ?*)

let %private
{unsafe_get = (.![]);
 unsafe_set = (.![]<-)} = (module Array)

let size ?(offs=0) ( x : t) : int =
  let acc = ref 1 in 
  for i = offs to Array.length x - 1 do 
    acc := !acc * x.![i]
  done;
  !acc   

let d1 x = [|x|]   
let d2 x y = [|x; y|]
let d3 x y z = [|x;y;z|]
let d4 a b c d = [|a;b;c;d|]

let dims = Array.length



let replace1D (x :t ) dim1 = 
  assert (dims x > 0);  
  let u = Array.copy x in 
  u.![0] <- dim1 ; 
  u

let replaceID (x : t) i dimi = 
  assert (dims x > i);
  let u = Array.copy x in 
  u.![i] <- dimi ;
  u
let removeID (x : t) i =
  let dim = dims x in   
  assert (dims x > i && i >=0 );  
  let u = Array.make (dim - 1) 0 in 
  for k = 0 to i - 1 do 
    u.![k] <- x.![k]  
  done;
  for k = i + 1 to dim - 1 do
    u.![k-1] <- x.![k]  
  done;
  u      

let eq (x : t) (y:t) = 
  Belt.Array.eqU x y (fun [@bs] x y -> x = y)  


let calc_stride (x : t) = 
  let len = Array.length x in 
  let u = Array.make len 1 in 
  for i = len - 2 downto 0 do 
    u.![i] <- u.![i+1] * x.![i+1]
  done ;
  u

(* TODO: optmize later using horner rule?
    The index length could be shorter than shape
*)  
let calc_offset (shape : t) (index : int array) =
  let stride = calc_stride shape in 
  let cursor = ref 0 in 
  for idim = 0 to Array.length index - 1 do 
    let shapevalue = shape.![idim] in 
    let ind = index.![idim] in 
    assert (ind < shapevalue);
    cursor := !cursor + ind * stride.![idim]
  done ;
  !cursor


let to_dimension (x : t) (dim : int) : t =   
  assert (dim >= 1);  
  let odim = Array.length x in
  let n = Array.make dim 1 in     
  if dim = odim then x else begin 
    if dim > odim then begin
      for i = 0 to odim - 1 do 
        n.![i+dim-odim] <- x.![i]
      done 
    end else begin
      for i = 0 to dim - 1 do 
        n.![i] <- x .![i + odim - dim]
      done   ;
      let acc = ref n.![0] in 
      for k = 0 to odim - dim - 1 do 
        acc := x.![k] * !acc  
      done ;
      n.![0] <- !acc 
    end;
    n
  end 
;;

(* check all elements starting from [off] to be 1 including off *)
let for_all_ones (arr : t) (off : int) (len : int) : bool = 
  let allones = ref true in 
  let len = off + len  in 
  let cur = ref off in  
  while !cur < len && !allones do
    (if arr.![!cur] <> 1 then 
       allones:=false);
    incr cur 
  done;
  !allones  
(**
   [
   | `same of t 
    (* the returned shape is useful to create a new tensor
      [2,3] [1,2,3] are considered to be the same shape 

    *)
    it returns (`same [1,2,3])
   | `a_has_b_as_suffix of int 
    (*
      [2,3,4] [3,4]
      [2,3,4] [1,3,4]
    *)
    it returns (`a_includes_b_as_suffix 1)
   | `a_has_b_as_prefix of int 
    (*
      [2,3,2] [2,3,1]
    *)
    it returns (`a_includes_b_as_prefix 2)
    | `b_has_a of int 
      [1,2,3] [2,2,3]
      `b_has_a 1 
   ]

   [1,1,2,3,1]  [1,1,2,3,2]
   [1,3]        [2,3]
   [2,1,3]      [2,2,3] -- not handled
   [2,1]        [2,2]

   [1,1,2,3] [1,2,2,3]
   -- this is an example that the first one is the same 
   -- does not indicate that it is a suffix or prefix
   [2,1]  [2,2]
   -- the first one is the same, it has to be prefix
   --
   [1,1,1,1,1]
   [1,1,2,1,1] -- considered either prefix/suffix
*)
type result = 
  [`a_has_b_prefix of int
  |`a_has_b_suffix of int
  | `b_has_a_prefix of int
  | `b_has_a_suffix of int
  | `mismatch
  | `same of t ]
let cat (shape1 : t) (shape2 : t) : result = 
  let cur = ref 0 in 
  let len = Array.length shape1 in 
  while !cur < len && shape1.![!cur] = shape2.![!cur] do 
    incr cur   
  done;
  if !cur = len then     
    `same shape1 
  else 
    let last = ref (len - 1) in 
    while !last > !cur && shape1.![!last] = shape2.![!last] do 
      decr last   
    done;
    if shape1.![!cur] = 1 then 
      if for_all_ones shape1 !cur (!last - !cur + 1) then 
        (* [1,1,  1,1,   2,3] [1,1,  2,2,  2,3] *)
        if for_all_ones shape1 0 !cur  then `b_has_a_suffix !last else 
          (* [2,2,  1,1,   2,3] [2,2,   2,2,  1,1] *)
        if for_all_ones shape1 (!last + 1) (len - !last - 1) then `b_has_a_prefix !cur 
        else `mismatch
      else 
        `mismatch 
    else if shape2.![!cur] = 1 then
      if for_all_ones shape2 !cur (!last - !cur + 1) then 
        if for_all_ones shape2 0 !cur then `a_has_b_suffix !last else 
        if for_all_ones shape2 (!last + 1) (len - !last - 1) then `a_has_b_prefix !cur 
        else `mismatch
      else `mismatch
    else `mismatch     
;;  

let classify  a b = 
  let size_a = dims a in 
  let size_b = dims b in 
  let cmp =  compare size_a size_b in 
  if cmp = 0 then 
    cat a b 
  else if cmp < 0 then 
    cat (a |. to_dimension size_b) (b)
  else 
    cat a (b |. to_dimension size_a)

