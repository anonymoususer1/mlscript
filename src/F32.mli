module B = F32Binding
type t = private {
  shape : TensorShape.t;   
  data : B.t 
}
type elt = float 

val from : elt array -> t
val from2D : elt array array -> t
val from3D : elt array array array -> t 
val zeros : TensorShape.t -> t 

val arange : ?step:int -> int -> int -> t

val size : t -> int
val toArray : t -> elt array
val shape : t -> TensorShape.t 

val toIntArray : t -> int array 

val dummy : t 

val reshape : t -> TensorShape.t -> t
val to_dimension : t -> int -> t 

val copy : t -> t

val copy_ : 
  ?out: t -> 
  t -> 
  t 

val sigmoid : t -> t
val exp : t -> t
val neg : t -> t
val as_scalar: t -> elt 
val dims : t -> int
val subarr : t -> int -> int -> t
val slice : t -> int array -> t 

val max : ?axis:int -> ?keepDims:bool -> t -> t
val min : ?axis:int -> ?keepDims:bool -> t -> t 
val argmax : ?axis:int -> t -> I32.t 
val argmin : ?axis:int -> t -> I32.t

val sum : ?axis:int -> ?keepDims:bool -> t -> t 
val sum_ :
  ?axis:int -> 
  ?out:t  -> 
  t  -> t 


val sum_all : t -> elt
val log : ?offset:elt -> t -> t 
val square_sum : ?axis:int -> t -> t 
val get : t -> int array -> elt
val set : t -> int array -> elt -> unit
val fill : t -> elt -> unit
val eq : t -> t -> bool
val neq : t -> t -> bool
val map_ : t -> (elt -> elt [@bs]) -> t
val map : t -> (elt -> elt [@bs]) -> t 
val reduce :
  ?axis : int -> 
  ?keepDims: bool -> 
  t -> (elt -> elt -> elt [@bs]) -> t 

(** Test on more dimensions*)  
val sampleUI8: 
  ?axis:int ->
  ?keepDims:bool ->
  t -> UI8.t -> t  

val fold : 
  ?axis : int ->
  ?init : elt -> 
  t -> (acc:elt -> elt -> elt [@bs]) -> t 

val forall: 
  t -> (elt -> bool [@bs]) -> bool 

val add : t -> t -> t 
val sub : t -> t -> t 
val mul : t -> t -> t 
val div : t -> t -> t

val add_ : 
  ?out:t ->
  t -> 
  t -> 
  t  

val sub_ : 
  ?out:t -> 
  t -> 
  t -> 
  t 

val mul_ :
  ?out:t ->
  t -> 
  t -> 
  t  

val div_ : 
  ?out:t ->
  t -> 
  t -> 
  t  

val dot : t -> t -> t 

val dot_:
  ?out:t -> 
  t -> 
  t -> 
  t 

val ( + ) : t -> t -> t 
val ( - ) : t -> t -> t 
val ( * ) : t -> t -> t 
val ( / ) : t -> t -> t
val ( @ )  : t -> t -> t 

val shuffle_dim0 : t -> t 
val softmax : t -> t 
val is_close : ?diff:elt -> t -> t -> bool 
val rand : TensorShape.t -> t 
val randn : TensorShape.t -> t 

val ofUI8 : UI8.t -> t 
val cross_entropy_error : t -> UI8.t ->  float
val transpose : t -> t 

val update_with_derivative: 
  ?lr:float -> 
  ?check_shape:bool ->
  t -> 
  t -> unit 

val apply_mask:
  ?check_shape:bool -> 
  t -> 
  t -> 
  unit  
val no_nan : t -> bool   


val multiply_add : 
  t -> 
  t -> 
  t -> 
  t 
val multiply_add_: 
  ?out:t -> 
  t -> 
  t -> 
  t -> t  

