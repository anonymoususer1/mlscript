
let %private {zeros_like; size} = (module Ndarray)
let %private {unsafe_get = (.![]); unsafe_set = (.![]<-)} = (module Array)


let numeric_gradient_no_batch f x =
  let h = 1e-4 in 
  let grad = zeros_like x in 
  let rawBuffer = x.data in
  for idx = 0 to size x - 1 do (* this is wrong*)
    let tmp_val =  rawBuffer.![idx] in 
    rawBuffer.![idx] <- tmp_val +. h ;
    let fxh1 = f x [@bs] in 
    rawBuffer.![idx] <- tmp_val -. h ;
    let fxh2 = f x [@bs] in 
    grad.data.![idx] <- (fxh1 -. fxh2) /. (2. *. h);
    rawBuffer.![idx] <- tmp_val
  done ;
  grad

(** TODO: 
   - support slice 
   -  slice set
   - slice update
   - broadcast
*)  
#if 0 then
let numeric_gradient f x = 
  if ndim x = 1 then 
    numeric_gradient_no_batch f x 
  else 
    let grad = zeros_like x in 
    let num = x.shape.![0] in 
    for idx = 0 to num - 1 do 
      grad.data.![idx] <- (numeric_gradient_no_batch f x.![idx]).data
    done ;
    grad  
#end