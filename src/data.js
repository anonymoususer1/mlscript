

var fs = require('fs')
var assert = require('assert')
var train_labels = fs.readFileSync('dataset/train-labels-idx1-ubyte').slice(8)
var train_images = fs.readFileSync('dataset/train-images-idx3-ubyte').slice(32)
assert (train_labels.length === 60_000)