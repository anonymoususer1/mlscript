type t

type elt = int 

external from : elt array -> t = "Int2Array" [@@new]
external make : int -> t = "Int32Array" [@@new]



include (BindingMake.Make(struct 
  type nonrec elt = elt 
  type nonrec typedarray = t
end))