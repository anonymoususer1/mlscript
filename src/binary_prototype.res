

module type Unary = {
  let ff : (.float) => float 
  let df : (.float, float) => float 
  let dr : (.float, float) => float   
}

module type Binary = {
  let ff : (.float, float) => float 
  let df : (.float,float,float,float) => float   
  let dr : (.float,float,float) => (float , float )
}

type rec t = 
  | DF ({ val : float , tangent: float})
  | DR ({ val : float, mutable adjoint : float, adj_fun : adj_fun })
and adj_fun = (.float, list < (float,t) >) => list <(float,t)>


let unary_op = (module (U :  Unary),x) => {
 switch x {
  | DF ({val,tangent}) => {
     let p' = U.ff(.val)
     let t  = U.df(.val,tangent)
     DF({val : p', tangent: t})
   }
  | DR ({val}) => {
    let p' = U.ff(.val)
    let adj_fun' = (.a,t)=>{
      let r = U.dr(.val ,a) 
      list{ (r,x), ...t}
    }
    DR({val : p', adjoint : 0. , adj_fun : adj_fun' })
  } 
 }
}

let binary_op = (module (B: Binary),xa,xb)=>{
  switch (xa,xb) {
    | (DF ({val:pa,tangent:ta}), DF({val:pb,tangent:tb}) )=>{
      let p' = B.ff(.pa,pb)
      let t' = B.df(.pa,pb,ta,tb)
      DF({val : p' , tangent : t'})
    }
   | (DR({val:pa}),DR ({val:pb})) => {
     let p' = B.ff(.pa,pb)
     let adj_fun = (.a,t) =>{
       let (ra,rb) = B.dr(. pa,pb,a)
       list{(ra,xa), (rb,xb), ... t}  
     }
     DR({val:p', adjoint :  0., adj_fun })
   }
   | (DR (_), DF (_))
   | (DF (_), DR (_)) => assert false
  }
}



let make_forward = (.p,a) => DF ({val : p, tangent : a})

let make_reverse = (.p) =>  DR({val : p, adjoint : 0., adj_fun : (. _, t)=> t })
  
let rec reverse_push = (.xs) =>{
  switch  xs {
  | list{} => ()
  | list{(v,x),...t} => {
    switch x {
      | DF (_) => assert false 
      | DR (p) => {
        p.adjoint = p.adjoint  +.  v 
        let stack = p.adj_fun(.p.adjoint,t)
        reverse_push(.stack)
      }
    }
  }
  }
}

let tangent = (.u : t)=> {
  switch u {
  | DF ({tangent}) => tangent
  | DR (_) => assert false
  }
}

let adjoint = (.u:t) => {
  switch  u {
  | DR ({adjoint}) => adjoint
  | DF (_) => assert false
  }
}
let diff = (.f) : ((.(t,t)) => float) => {
  (.x) => {
    switch x {
    | (DF (_), DF (_)) =>
      tangent(.f(.x))
    | (DR (_), DR (_)) => {
      let r : t = f (.x )
      reverse_push(.list{(1.,r)})
      adjoint(.fst(x))
    }  
    | _ => assert false
    }
  }
}

module Sin = {
  let ff =(.x)=> sin(x)
  let df =(.v,d) => d *. cos(v)  
  let dr = (.v,a) => a *. cos (v)
}

module Add = {
  let ff = (.x,y)=> x +. y 
  let df = (. _, _,ta,tb) => ta +. tb 
  let dr = (._,_,a) =>(a,a)
}


let sin_ad = unary_op (module ({ 
  let ff =(.x)=> sin(x)
  let df =(.v,d) => d *. cos(v)  
  let dr = (.v,a) => a *. cos (v)})
)
let add_ad = binary_op(module (Add))
