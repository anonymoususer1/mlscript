function initShader(){

    var canvas = document.createElement('canvas');
    // @ts-ignore
    canvas.setAttribute('width', 512);
    // @ts-ignore
    canvas.setAttribute('height', 512);

    var gl = canvas.getContext('webgl2');
    //gl.disable(this.gl.DITHER);

    // console.log(gl)

    return gl

}

function pre_matrix_operation(){
    var program = gl.useProgram();

}

function createTensorTexture(gl, data, width, height){
    //console.log("texture");
    var texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, width, height, 0, gl.RGBA, gl.FLOAT, data);

    return texture;
}

function binary_makeShader(gl, shaderArray, ops, uniformsNames, ops_type){
    // build_matrixShaderProgram

    var shaderInfoDict = {};

     var shaderSource = ``;

    if (ops_type === 'binary'){
        n = 2;
    }
    else if(ops_type === 'binary'){
        n = 1;
    }

    for (var i=0; i<2; ++i){
        shaderSource += `uniform sampler2D samplerTexture` + i.toString() + `; \n`
    }

    var shaderProgram=build_shaderProgram(shaderSource, ops);

    if (Object.keys(shaderInfoDict).length===0){
        var AttributePointer = gl.getAttribLocation(shaderProgram, 'position');
        gl.enableVertexAttribArray(AttributePointer);

        create_andBindVBOs(AttributePointer);
    }

    for (var i=0, uniformsInputMatrices=[]; i<n; ++i){
        uniformsInputMatrices.push(gl.getUniformLocation(shaderProgram, 'samplerTexture' + i.toString()));
    }

    var uniformsDict={};
    if (uniformsGLSLnames){
        uniformsGLSLnames.forEach(function(uniformGLSLname){
            uniformsDict[uniformGLSLname]=gl.getUniformLocation(shaderProgram, uniformGLSLname);
        });
    }

    shaderInfoDict[ops]={
        uniformResolution: gl.getUniformLocation(shaderProgram, 'resolution'),
        uniformsInputMatrices: uniformsInputMatrices,
        shaderProgram: shaderProgram,
        uniforms: uniformsDict
    };

    gl.useProgram(shaderProgram);
    uniformsInputMatrices.forEach(function(uniformMat, uniformMatIndex){
        gl.uniform1i(uniformMat, uniformMatIndex);
    });

}



function shaderCompiler(gl, shader_type, shader_source, typeString){

    var shader = gl.createShader(shader_type);

    gl.shaderSource(shader, shader_source);
    gl.compileShader(shader);

    // debug glsl
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("ERROR IN "+typeString+ " SHADER : " + gl.getShaderInfoLog(shader));
        console.log('Buggy shader source : \n', source);
        return false;
    }

    return shader;

};

function build_shaderProgram(gl, fragmentShaderSource, ops){
    var vertexShaderSource = vertexShader()
    
    var vertexShader = shaderCompiler(gl, gl.VERTEX_SHADER, vertexShaderSource, "VERTEX"+ops);

    var fragmentShader = shaderCompiler(gl, gl.VERTEX_SHADER, fragmentShaderSource, "FRAGMENT"+ops);


    var shaderProgram=gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);

    gl.linkProgram(shaderProgram);

    return shaderProgram;
}

function create_andBindVBOs(gl, positionAttributePointer){
    var quadVertices = new Float32Array([
        -1, -1, //bottom left corner 
        -1, 1,  //top left corner    
        1, 1,   //top right corner   
        1, -1  //bottom right corner 
    ]);
    var quadIndices = new Uint16Array([
        0,1,2, 
        0,2,3  
    ]);

    //send vertices to the GPU :
    var quadVerticesVBO= gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, quadVerticesVBO);
    gl.bufferData(gl.ARRAY_BUFFER, quadVertices, gl.STATIC_DRAW);

    //send indices to the GPU :
    var quadIndicesVBO= gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, quadIndicesVBO);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, quadIndices, gl.STATIC_DRAW);

    //BIND VBOs
    gl.bindBuffer(gl.ARRAY_BUFFER, quadVerticesVBO);
    gl.vertexAttribPointer(positionAttributePointer, 2, gl.FLOAT, false, 8,0);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, quadIndicesVBO); 
}

function compile_shader(gl, source, type, typeString){
    var shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    //debug glsl
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("ERROR IN "+typeString+ " SHADER : " + gl.getShaderInfoLog(shader));
        console.log('Buggy shader source : \n', source);
        return false;
    }
    return shader;
};

function pre_matrix_operation(gl, glProgram, ops, width, height){
    gl.useProgram(ops, glProgram);
    gl.uniform2f(width, height);
}

function shaderUniform(ops, uniformName, shaderInfoDict){
    reutrn shaderInfoDict[ops].uniformName;
}

function matMultiplyShaderProgram(){


}

function draw_elements(gl){
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);

}


