/*
Tensor Libaray Test
matrix multiply
*/


var range = function(start, stop, step){
    step = step || 1;
    var arr = [];
    for (var i=start;i<stop;i+=step){
       arr.push(i);
    }
    return arr;
};




function main(){
    var dim = 100;
    a_element = range(0, dim*dim, 1);

    var a_gpu=new mlscript.tensor(dim,dim,[a_element]);
    var b_gpu=new mlscript.tensor(dim,dim, [a_element]);

    var r_gpu=new mlscript.zeros(dim, dim);


    var t_large1 = performance.now()
    a_gpu.matmultiply(b_gpu, r_gpu); //do matrix operation A*B and put the result to R
    var t_large2 = performance.now()
    console.log('TEST MULTIPLY TWO ${dim} by ${dim} MATRICES ON GPU ` + (t_large2 - t_large1) + " milliseconds.");

    a = [a_element];
    b = [a_element];
    a.reshape(dim, dim);
    b.reshape(dim, dim);
    
    console.log('\nMatrix B = ')
    
    /*
    var t6 = performance.now();
    var r = multiply(a, b);
    var t7 = performance.now();
    console.log("TEST MULTIPLY TWO 5 X 5 MATRICES ON CPU " + (t7 - t6) + " milliseconds.")

    */
      
    }
    


