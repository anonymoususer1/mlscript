function vertexShader(){
    return `
    precision highp float;
    attribute vec2 posiiton;

    void main(void){
        gl_Position=vec4(position, 0., 1.);
    }
    `
}


function addShader(){
    return `
    precision highp float;
    uniform vec2 resolution;
    uniform sampler2D samplerTexture0;
    uniform sampler2D samplerTexture1;

    void main(void){
        vec2 uv = gl_FragCoord.xy/resolution;
        vec4 getMatrixA = texture2D(samplerTexture0, uv);
        vec4 getMatrixB = texture2D(samplerTexture0, uv);
        gl_FragColor = getMatrixA + getMatrixB;
    }
    `
}


function multiplyShader(dim){

    return `
    precision highp float;

    const vec2 matrixA=vec2(1/${dim}+0.);
    const vec2 matrixB=vec2(1/${dim}+0.);

    void main(void){
        uniform vec2 resolution;
        uniform sampler2D samplerTexture0;
        uniform sampler2D samplerTexture1;
        vec2 uv = gl_FragCoord.xy/resolution;

        vec2 mask1= uv*vec2(1., 0.);
        vec2 mask2= uv*vec2(0., 1.);

        for (float i=0; i<${dim}; i=i+1.0){
            result+=texture2D(samplerTexture0, mask1+(i)*matrixA);
        }

        gl_FragColor = result;
    }

    
    `


}

function printShader(){
    // print shader Source: https://stackoverflow.com/questions/17981163/webgl-read-pixels-from-floating-point-render-target
    return `
    float shift_right (float v, float amt) { 
        v = floor(v) + 0.5; 
        return floor(v / exp2(amt)); 
    }
    float shift_left (float v, float amt) { 
        return floor(v * exp2(amt) + 0.5); 
    }
    float mask_last (float v, float bits) { 
        return mod(v, shift_left(1.0, bits)); 
    }
    float extract_bits (float num, float from, float to) { 
        from = floor(from + 0.5); to = floor(to + 0.5); 
        return mask_last(shift_right(num, from), to - from); 
    }
    vec4 encode_float (float val) { 
        if (val == 0.0) return vec4(0, 0, 0, 0); 
        float sign = val > 0.0 ? 0.0 : 1.0; 
        val = abs(val); 
        float exponent = floor(log2(val)); 
        float biased_exponent = exponent + 127.0; 
        float fraction = ((val / exp2(exponent)) - 1.0) * 8388608.0; 
        float t = biased_exponent / 2.0; 
        float last_bit_of_biased_exponent = fract(t) * 2.0; 
        float remaining_bits_of_biased_exponent = floor(t); 
        float byte4 = extract_bits(fraction, 0.0, 8.0) / 255.0; 
        float byte3 = extract_bits(fraction, 8.0, 16.0) / 255.0; 
        float byte2 = (last_bit_of_biased_exponent * 128.0 + extract_bits(fraction, 16.0, 23.0)) / 255.0; 
        float byte1 = (sign * 128.0 + remaining_bits_of_biased_exponent) / 255.0; 
        return vec4(byte4, byte3, byte2, byte1); 
    }
    
     // (the following inside main(){}) return your float as the fragment color
     float myFloat = 420.420;
     gl_FragColor = encode_float(myFloat);
    `
}

function multiplyScalarShader(){
    return `
    precision highp float;
    uniform vec2 resolution;
    uniform float scalar;
    uniform sampler2D samplerTexture0;
    uniform sampler2D samplerTexture1;

    void main(void){
        vec2 uv = gl_FragCoord.xy/resolution;
        gl_FragColor=texture2D(samplerTexture0, uv) * scalar;
    }
    `
}

function customizeFunctionShader(sourceCode){
    return `
    precision highp float;
    void main(void){
        vec2 uv = gl_FragCoord.xy / resolution;
        vec4 x = texture2D(samplerTexture0, uv);
        vec4 functions;
        ${sourceCode}
        gl_FragColor = functions;
    }

    `
}

function transposeShader(){
    return `
    precision highp float;
    
    void main(void){
        uniform vec2 resolution;
        vec2 uv = gl_FragCoord.xy / resolution;
        
        gl_FragColor = texture2D(samplerTexture0, uv.yx);
    }
    `
}