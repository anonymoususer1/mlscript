//@ts-check
// @ts-ignore
function tensor(Data, shape){
    // @ts-ignore
    var gl = initShader();
    var shaderInfoDict = {}
    console.log(gl)
    gl.disable(gl.DITHER);
    var RTTFBO = gl.createFramebuffer();
   
    var width = shape[0];
    var height = shape[1];
    var n_total = height * width * 4;

    var data = new Float32Array(n_total);

    for (var i=0; i<width*height; ++i){
        var n = i * 4;
        data[n]   = Data[i];
        data[n+1] = Data[i];
        data[n+2] = Data[i];
        data[n+3] = Data[i];
    }

    this.data = data
    this.shape = shape;

    // @ts-ignore
    var matrixTexture = createTensorTexture(gl, data, width, height);
    
    var bufferByte=[new Uint8Array(n_total), new Uint8Array(n_total), new Uint8Array(n_total), new Uint8Array(n_total)];

    for (var i=0, bufferFloat=[]; i<4; ++i){
        bufferFloat.push(new Float32Array(bufferByte[i].buffer));
    }

    this.print=function(){
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        pre_matrix_operation('PRINT', [that], width, height);

        var channel_mask=[
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ];

        for (var i=0; i<4; ++i){
            gl.viewport(0, width * i, height, width);
            gl.uniform4fv(shaderUniform('PRINT', 'colorChannelMask'), channel_mask[i]);

            gl.uniform2f(shaderUniform('PRINT', 'uvOffset'),0, -width * i);
            fill_viewport();

            gl.readPixels(0, width *i, height, width, gl.RGBA, gl.UNSIGNED_BYTE, bufferByte[i])
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER, RTTFBO);

        bufferFloat[0].shape=[width, height];


        return bufferFloat[0];

    }
    
    this.add=function(matrixB){
        // @ts-ignore
        var r = new zeros(matrixB.shape[0]*matrixB.shape[1]);

        return r

    }

    this.matmultipky=function(matrixB, matrixR){
        if (height!==matrixB.shape[0]){
            throw 'cannot multiply : dimensions mismatch';
        }
        var shaderOps='MULTIPLY'+height.toString();
        
        matMultiplyShaderProgram(height);
        

        return pre_matrix_operation(shaderOps, matrixB, matrixR);
    }

    this.wxb=function(matrixB, matrixC, matrixR){
        if (height!==matrixB.shape[0]){
            throw 'dimension miss match';
        }
        var shaderOps='wxb'+height.toString();
        
        matrixMultiplyShaderProgram(height);
        
        return pre_matrix_operation(shaderOps, matrixC, matrixB, matrixR);
    }

    this.multiplyScalar=function(scalar, matrixR){
        return matrixOperation('MULTIPLYSCALAR', [self], matrixR, function(){
            gl.uniform1f(shaderUniform('MULTIPLYSCALAR', 'scalar'), scalar);
        });
    }

    this.apply=function(name, matrixR){
        var shaderOps='FUNC'+name;
        if (!matrixShaderProgram(shaderOps)){
            throw 'Cannot find function '+name+' . Plz add it using WGLMatrix.addFunction(...)';
        }
        return pre_matrix_operation(shaderId, [self], matrixR);
    }

    this.copy=function(matrixR){
        return pre_matrix_operation('COPY', [self], matrixR);
    }


}

function zeros(nRows, height){
    var flattenData = new Float32Array(nRows * height);
    var arr = new tensor(flattenData, [width, height]);

    return arr
}