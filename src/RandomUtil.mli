
val randint : int -> int -> int 
(* uniform distribution *)
val rand : unit -> float 
(* normal distribution with std = 0, mean = 1. *)
val randn : unit -> float 