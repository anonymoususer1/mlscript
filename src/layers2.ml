
[@@@config { flags = [|"-dsource"|]}]

[@@@warning "-44"]


class type objlayer = object 
  method forward : F64.t -> F64.t 
  method backward : F64.t -> F64.t 
  method update : unit -> unit 
end 

class type lastlayer = object 
  method forward : F64.t -> F64.t -> float 
  method backward : float -> F64.t 
end 


type model = {
  layers : objlayer array;
  last : lastlayer
}

let loss ({layers ; last} : model) ~input ~label  : float = 
  let input = ref input in 
  for i = 0 to Array.length layers - 1 do 
    input := layers.(i)##forward (!input)  
  done ;
  last##forward !input label


let gradient ({layers ; last}  as  m : model) ~input ~label  = 
  let l = loss m ~input ~label in 
  let g = ref (last##backward 1.) in 
  for i = Array.length layers - 1 downto 0 do 
    g := layers.(i)##backward !g
  done;         
  l 

let update ({layers} : model) = 
  for i = 0 to Array.length layers - 1 do 
    layers.(i)##update ()  
  done     


(** providing layers below *)
let sigmoid  () : objlayer = object (self)
  val mutable out = F64.dummy
  method forward input = 
    out <- F64.sigmoid input; 
    self#out 
  method backward dout = 
    F64.(dout * (self#out |. map (fun [@bs] x -> x *. (1. -. x )) ))   
  method update () = ()
end 


let relu () : objlayer = object(self)
  val mutable mask = F64.dummy 
  method forward input = 
    let out = input |. F64.map (fun [@bs] x -> if x > 0. then x else 0.) in 
    mask <- input;
    out
  method backward dout = 
    F64.apply_mask dout self#mask ;
    dout   
  method update () = ()
end 
let corss_entroy_error (y : F64.t) (t : F64.t) : float = 
  let batch_size = float y.shape.(0) in 
  - 1. /. batch_size *. F64.(sum_all  (t * log y))

let softmax_with_loss ()  : lastlayer = object(self)
  val mutable loss  = 0.
  val mutable y = F64.dummy
  val mutable t = F64.dummy
  method forward input t = 
    t <- t ;
    y <- F64.softmax input; 
    loss <- corss_entroy_error self#y self#t ;
    self#loss 
  method backward dout =
    let batch_size = float (self#t).F64.shape.(0) in 
    F64.( (self#y - self#t) |.map_ (fun[@bs] x -> dout *. x /. batch_size))    
end

let affine w b : objlayer = object (self)
  val mutable w = w 
  val mutable b = b 
  val mutable input = F64.dummy 
  val mutable dw = F64.dummy
  val mutable db = F64.dummy 
  method forward input = 
    input <- input ;
    F64.multiply_add input w b
  method backward dout = let open F64 in 
    dw <- transpose self#input @ dout;
    db <- F64.sum dout ~axis:0 ~keepDims:false;
    dout @ transpose self#w
  method update () = 
    let open F64 in 
    update_with_derivative self#w self#dw;
    update_with_derivative self#b self#db
end 


let two_layer ~w1 ~b1 ~w2 ~b2 : model = 
  let affine1 = affine w1 b1 in 
  (* let sigmoid = sigmoid () in  *)
  let sigmoid = relu () in 
  let affine2 = affine w2 b2 in 
  let lastlayer = softmax_with_loss () in 
  {layers =  [|affine1; sigmoid; affine2 |]; last = lastlayer}

let {push } = (module Js.Array2)




let {label_tensor ; train_tensor; w1; w2; b1; b2} = 
  (module Data_provider.F64)
let nn = two_layer ~w1 ~b1 ~w2 ~b2 
let train (train_tensor : F64.t) (label_tensor : F64.t)  = 
  let losses = [||] in 
  let total_size =  train_tensor.shape.(0) in    
  for i = 0 to total_size / 100 - 1 do 
    let input = F64.subarr train_tensor (i * 100)  100 in 
    let label =  F64.subarr label_tensor (i * 100) 100 in      
    let loss = nn  |. gradient ~input ~label in 
    nn |. update ;
    losses |. push loss |. ignore ; 
  done;
  losses    

;; Js.Console.timeStart "training"  
;; Js.log (train train_tensor label_tensor)
;; Js.Console.timeEnd "training"