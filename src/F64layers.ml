
[@@@config { flags = [|"-dsource"|]}]

[@@@warning "-44"]

module F = F64
type tensor  =  F.t

class type layer = object 
  method forward : tensor -> tensor 
  method backward : tensor -> tensor 
  method update : unit -> unit 
end 

class type lastlayer = object 
  method forward : tensor -> tensor -> float 
  method backward : float -> tensor 
end 


type model = {
  layers : layer array;
  last : lastlayer; 
  name : string;
}

let loss ({layers ; last} : model) ~input ~label  : float = 
  let input = ref input in 
  for i = 0 to Array.length layers - 1 do 
    input := layers.(i)##forward (!input)  
  done ;
  last##forward !input label


let gradient ({layers ; last}  as  m : model) ~input ~label  = 
  let l = loss m ~input ~label in 
  let g = ref (last##backward 1.) in 
  for i = Array.length layers - 1 downto 0 do 
    g := layers.(i)##backward !g
  done;         
  l 

let update ({layers} : model) = 
  for i = 0 to Array.length layers - 1 do 
    layers.(i)##update ()  
  done     


(** providing layers below *)
let sigmoid  () : layer = object (self)
  val mutable out = F.dummy
  method forward input = 
    out <- F.sigmoid input; 
    self#out 
  method backward dout = 
    F.(dout * (self#out |. map (fun [@bs] x -> x *. (1. -. x )) ))   
  method update () = ()
end 

let relu () : layer = object(self)
  val mutable mask = F.dummy   
  method forward input = 
    let out = input |. F.map (fun [@bs] x -> if x > 0. then x else 0.) in 
    mask <- input;
    out
  method backward dout = 
    F.apply_mask dout self#mask ;
    dout   
  method update () = ()
end 

let affine w b : layer = object (self)
  val mutable w = w 
  val mutable b = b 
  val mutable input = F.dummy 
  val mutable dw = F.dummy
  val mutable db = F.dummy 
  method forward input = 
    input <- input ;
    F.((input @ w) + b)
  method backward dout = let open F in 
    dw <- transpose self#input @ dout;
    db <- F.sum dout ~axis:0 ~keepDims:false;
    dout @ transpose self#w
  method update () = 
    let open F in 
    update_with_derivative self#w self#dw;
    update_with_derivative self#b self#db
end 

let relu2 () : layer = object(self)
  val mutable mask = F.dummy   
  method forward input = 
    let out = input |. F.map (fun [@bs] x -> if x > 0. then x else 0.) in 
    mask <- input;
    out
  method backward dout = 
    F.apply_mask dout self#mask ;
    dout   
  method update () = ()
end 

let corss_entroy_error (y : tensor) (t : tensor) : float = 
  let batch_size = float y.shape.(0) in 
  - 1. /. batch_size *. F.(sum_all  (t * log y))

let softmax_with_loss ()  : lastlayer = object(self)
  val mutable loss  = 0.
  val mutable y = F.dummy
  val mutable t = F.dummy
  method forward input t = 
    t <- t ;
    y <- F.softmax input; 
    loss <- corss_entroy_error self#y self#t ;
    self#loss 
  method backward dout =
    let batch_size = float (self#t).F.shape.(0) in 
    F.( (self#y - self#t) |.map_ (fun[@bs] x -> dout *. x /. batch_size))    
end

let softmax_with_loss2 ()  : lastlayer = object(self)
  val mutable loss  = 0.
  val mutable y = F.dummy
  val mutable t = F.dummy
  val mutable backward_out = None 
  method forward input t = 
    t <- t ;
    y <- F.softmax input; 
    loss <- corss_entroy_error self#y self#t ;
    self#loss 
  method backward dout = 
    let batch_size = float (self#t).F.shape.(0) in 
    let backward_out  = F.sub_ ?out:self#backward_out self#y self#t in 
    backward_out <- Some backward_out; 
    backward_out |.
    F.map_ (fun[@bs] x -> dout *. x /. batch_size)
end





let affine2 w b : layer = object (self)
  val mutable w = w 
  val mutable b = b 
  val mutable input = F.dummy 
  val mutable dw = F.dummy
  val mutable db = F.dummy 
  method forward input = 
    input <- input ;
    F.multiply_add input w b
  method backward dout = let open F in 
    dw <- transpose self#input @ dout;
    db <- F.sum dout ~axis:0 ~keepDims:false;
    dout @ transpose self#w
  method update () = 
    let open F in 
    update_with_derivative self#w self#dw;
    update_with_derivative self#b self#db
end 

let affine3 w b : layer = object (self)
  val mutable w = w 
  val mutable b = b 
  val mutable input = F.dummy 
  val mutable dw = None
  val mutable db = None
  val mutable out = None (* avoid reallocation*)
  method forward input = 
    input <- input ;
    let out = F.multiply_add_ ?out:self#out input w b in 
    out <- Some out;
    out
  method backward dout = let open F in     
    dw <- Some (F.dot_ ?out:self#dw (transpose self#input) dout);
    db <- Some (F.sum_ ?out:self#db dout ~axis:0); 
    dout @ transpose self#w
  method update () = 
    let open F in 
    update_with_derivative self#w (Belt.Option.getExn self#dw);
    update_with_derivative self#b (Belt.Option.getExn self#db)
end 


(** insight : the shape of tensor is fixed for our models 
    so we can reuse the memory 
*)
let affine4 w b : layer = object (self)
  val mutable w = w 
  val mutable b = b 
  val mutable input = F.dummy 
  val mutable dw = None
  val mutable db = None
  val mutable forward_out = None (* avoid reallocation*)
  val mutable backward_out = None 
  method forward input = 
    input <- input ;
    F.multiply_add_ ?out:self#forward_out input w b  
  method backward dout = let open F in 
    dw <- Some (F.dot_ ?out:self#dw (transpose self#input) dout);
    db <- Some (F.sum_ ?out:self#db dout ~axis:0); 
    F.dot_ ?out:self#backward_out dout  (transpose self#w)
  method update () = 
    let open F in 
    update_with_derivative self#w (Belt.Option.getExn self#dw);
    update_with_derivative self#b (Belt.Option.getExn self#db)
end 



let {push } = (module Js.Array2)




let {label_tensor ; train_tensor; w1; w2; b1; b2} = 
  (module Data_provider.F64)

let nn =
  {
    layers =  
      [|
        affine w1 b1; 
        relu (); 
        affine w2 b2 
      |]; 
    last = 
      softmax_with_loss (); 
    name = "model1"
  }    

let nn2 = 
  {layers =  [|affine2 w1 b1; relu (); affine2 w2 b2 |]; 
   last = softmax_with_loss (); name = "model2"}  

let nn3 = 
  {layers =  [|affine3 w1 b1; relu (); affine3 w2 b2 |]; 
   last = softmax_with_loss (); name = "model3"}  


let nn4 = 
  {layers =  [|affine4 w1 b1; relu (); affine4 w2 b2 |]; 
   last = softmax_with_loss (); name = "model4"}  


let nn5 = 
  {layers =  [|affine4 w1 b1; relu (); affine4 w2 b2 |]; 
   last = softmax_with_loss2 (); name = "model5"}  

let train (train_tensor : tensor) (label_tensor : tensor) (model : model) = 
  Js.Console.timeStart model.name ;   
  let losses = [||] in 
  let total_size =  train_tensor.shape.(0) in    
  for i = 0 to total_size / 100 - 1 do 
    let input = F.subarr train_tensor (i * 100)  100 in 
    let label =  F.subarr label_tensor (i * 100) 100 in      
    let loss = model  |. gradient ~input ~label in 
    model |. update ;
    losses |. push loss |. ignore ; 
  done;
  Js.Console.timeEnd model.name;
  losses    



;;  SimdSetup.setup () |> Js.Promise.then_(fun _ -> 

    (* ;; Js.log (train train_tensor label_tensor nn) *)
    Js.log 
      (train train_tensor label_tensor nn5 |. Js.Array2.slice ~start:0 ~end_:10);
    Js.Promise.resolve ()  
  ) |. ignore 
