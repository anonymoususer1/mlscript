let %private {
  unsafe_get = ( .![] ) ;
  unsafe_set = ( .![]<-);
} = (module Array)



let size_of_shape ?(offs=0) ( x : int array) : int =
  let acc = ref 1 in 
  for i = offs to Array.length x - 1 do 
    acc := !acc * x.![i]
  done;
  !acc   

(** shape is small *)  
let eq_shape (x : int array) (y : int array) = 
  Belt.Array.eqU x y (fun [@bs] x y -> x = y)
  
let [@inline] calc_index_no_check 
    ~(stride : int array) 
    (index : int array) 
  = 
  let cursor = ref 0 in 
  for idim = 0 to Array.length stride - 1 do 
    let ind = index.![idim] in 
    cursor := !cursor + ind * stride.![idim]
  done ;
  !cursor
let stride_of_shape (x : int array) = 
  let len = Array.length x in 
  let u = Array.make len 1 in 
  for i = len - 2 downto 0 do 
    u.![i] <- u.![i+1] * x.![i+1]
  done ;
  u


let [@inline] calc_index_and_check 
    (index : int array) 
    (shape : int array) 
    (stride : int array) = 
  let cursor = ref 0 in 
  for idim = 0 to Array.length shape - 1 do 
    let shapevalue = shape.![idim] in 
    let ind = index.![idim] in 
    assert (ind < shapevalue);
    cursor := !cursor + ind * stride.![idim]
  done ;
  !cursor

