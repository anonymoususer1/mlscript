(** set up the SIMD support *)

external setup : unit -> unit Js.Promise.t  = "polyfill"
[@@module "./wasm/main.js"] [@@val]