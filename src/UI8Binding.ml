type t

type elt = int (* reading elements are still int *)

external from : elt array -> t = "Uint8Array" [@@new]
external make : int -> t = "Uint8Array" [@@new]



include (BindingMake.Make(struct 
  type nonrec elt = elt 
  type nonrec typedarray = t
end))