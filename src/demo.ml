


let sigmoid x = 
  x |. F64.map (fun[@bs] x -> 1. /. +. exp (-. x ))