#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>


EMSCRIPTEN_KEEPALIVE
double dot(double *a, double *b, int len)
{
    double sum = 0;
    for (int i = 0; i < len; ++i)
    {
        sum += a[i] * b[i];
    };
    return sum ;
}

// EMSCRIPTEN_KEEPALIVE
double *create_buffer(int len)
{
    return malloc(len * sizeof(double));
}

// EMSCRIPTEN_KEEPALIVE
void destroy_buffer(double *p)
{
    free(p);
}


float sum_aux_float(float *t, int offa, float *h, int offb, int strideb, int len)
{
    float result = 0.;
    int cursora = offa;
    int cursorb = offb;
    for (int i = 0; i < len; ++i)
    {
        result += t[cursora] * h[cursorb];
        ++cursora;
        cursorb += strideb;
    }
    return result;
}

double sum_aux(double *t, int offa, double *h, int offb, int strideb, int len)
{
    double result = 0.;
    int cursora = offa;
    int cursorb = offb;
    for (int i = 0; i < len; ++i)
    {
        result += t[cursora] * h[cursorb];
        ++cursora;
        cursorb += strideb;
    }
    return result;
}

/**
 *  x * y^T
 */ 
EMSCRIPTEN_KEEPALIVE
void mm_transpose(double *result_data, double *x_data, double *y_data, int stride_a, int stride_b, int len)
{
    double sum = 0.;     
    for (int i = 0; i < stride_a; ++i)
    {
        for (int j = 0; j < stride_b; ++j)
        {
            sum = 0.; 
            double * x_offset = x_data + i * len ;
            double * y_offset = y_data + j * len;
            result_data [ i * stride_b + j] = dot(x_offset,y_offset, len) ;
        }
    }
}

EMSCRIPTEN_KEEPALIVE
void mm(double *result_data, double *x_data, double *y_data, int stride_a, int stride_b, int len)
{
    int offa = 0;
    int offb = 0;
    int cursor = 0;
    for (int i = 0; i < stride_a; ++i)
    {
        for (int j = 0; j < stride_b; ++j)
        {
            result_data[cursor] = sum_aux(x_data, offa, y_data, offb, stride_b, len);
            ++cursor;
            ++offb;
        }
        offb = 0;
        offa += len;
    }
}


EMSCRIPTEN_KEEPALIVE
void mm_float(float *result_data, float *x_data, float *y_data, int stride_a, int stride_b, int len)
{
    int offa = 0;
    int offb = 0;
    int cursor = 0;
    for (int i = 0; i < stride_a; ++i)
    {
        for (int j = 0; j < stride_b; ++j)
        {
            result_data[cursor] = sum_aux_float(x_data, offa, y_data, offb, stride_b, len);
            ++cursor;
            ++offb;
        }
        offb = 0;
        offa += len;
    }
}

EMSCRIPTEN_KEEPALIVE
void mm_add(double *result_data, double *x_data, double *y_data, int stride_a, int stride_b, int len, double *offset)
{
    int offa = 0;
    int offb = 0;
    int cursor = 0;
    for (int i = 0; i < stride_a; ++i)
    {
        for (int j = 0; j < stride_b; ++j)
        {
            double offs = offset[j];
            result_data[cursor] = 
                sum_aux(x_data, offa, y_data, offb, stride_b, len) + offs;
            ++cursor;
            ++offb;
        }
        offb = 0;
        offa += len;
    }
}

EMSCRIPTEN_KEEPALIVE
void mm_add_float(float *result_data, float *x_data, float *y_data, int stride_a, int stride_b, int len, float *offset)
{
    int offa = 0;
    int offb = 0;
    int cursor = 0;
    for (int i = 0; i < stride_a; ++i)
    {
        for (int j = 0; j < stride_b; ++j)
        {
            float offs = offset[j];
            result_data[cursor] = 
                sum_aux_float(x_data, offa, y_data, offb, stride_b, len) + offs;
            ++cursor;
            ++offb;
        }
        offb = 0;
        offa += len;
    }
}