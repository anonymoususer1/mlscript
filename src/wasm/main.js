// https://github.com/emscripten-core/emscripten/issues/5352
// sync setup
// https://github.com/kripken/emscripten/wiki/WebAssembly#wasm-files-and-compilation

async function polyfill() {
  /**
   * @type { (out: Float64Array, a : Float64Array, b : Float64Array, stridea : number, strideb: number , len : number) => void}
   */

  var Module = await require("./a.out.js")();
  /**
   *
   * @param {Float64Array} arr
   * @returns {number} returned a pointer
   */
  function sendToWasmF64(arr) {
    var size = arr.length;
    var elt_size = arr.BYTES_PER_ELEMENT;
    var offset = Module._malloc(size * elt_size);
    Module.HEAPF64.set(arr, offset / elt_size);
    return offset;
  }

  /**
   *
   * @param {Float64Array} arr
   * @returns {number} returned a pointer
   */
  function sendToWasmF32(arr) {
    var size = arr.length;
    var elt_size = arr.BYTES_PER_ELEMENT;
    var offset = Module._malloc(size * elt_size);
    Module.HEAPF32.set(arr, offset / elt_size);
    return offset;
  }

  /**
   * @type { (out: Float64Array, a : Float64Array, b : Float64Array, stridea : number, strideb: number , len : number) => void}
   */
  globalThis.float64_mm = function (out, ma, mb, stridea, strideb, len) {
    var wa = sendToWasmF64(ma);
    var wb = sendToWasmF64(mb);
    var wc = Module._malloc(out.length * 8);
    Module._mm(wc, wa, wb, stridea, strideb, len);
    out.set(Module.HEAPF64.subarray(wc / 8, wc / 8 + out.length));
    Module._free(wa);
    Module._free(wb);
    Module._free(wc);
  };

  /**
   * @type { (out: Float64Array, a : Float64Array, b : Float64Array, stridea : number, strideb: number , len : number) => void}
   */
  globalThis.float64_mm_transpose = function (out, ma, mb, stridea, strideb, len) {
    var wa = sendToWasmF64(ma);
    var wb = sendToWasmF64(mb);
    var wc = Module._malloc(out.length * 8);
    Module._mm_transpose(wc, wa, wb, stridea, strideb, len);
    out.set(Module.HEAPF64.subarray(wc / 8, wc / 8 + out.length));
    Module._free(wa);
    Module._free(wb);
    Module._free(wc);
  };
  /**
   * @type { (out: Float32Array, a : Float32Array, b : Float32Array, stridea : number, strideb: number , len : number) => void}
   */
  globalThis.float32_mm = function (out, ma, mb, stridea, strideb, len) {
    var elt_size = out.BYTES_PER_ELEMENT;
    var wa = sendToWasmF32(ma);
    var wb = sendToWasmF32(mb);
    var wc = Module._malloc(out.length * elt_size);
    Module._mm_float(wc, wa, wb, stridea, strideb, len);
    out.set(Module.HEAPF32.subarray(wc / elt_size, wc / elt_size + out.length));
    Module._free(wa);
    Module._free(wb);
    Module._free(wc);
  };
  /**
   * @type { (out: Float64Array, a : Float64Array, b : Float64Array, stridea : number, strideb: number , len : number,offs : Float64Array) => void}
   */
  globalThis.float64_mm_add = function (
    out,
    ma,
    mb,
    stridea,
    strideb,
    len,
    offs
  ) {
    var elt_size = out.BYTES_PER_ELEMENT;
    var wa = sendToWasmF64(ma);
    var wb = sendToWasmF64(mb);
    var wd = sendToWasmF64(offs);
    var wc = Module._malloc(out.length * elt_size);
    Module._mm_add(wc, wa, wb, stridea, strideb, len, wd);
    out.set(Module.HEAPF64.subarray(wc / elt_size, wc / elt_size + out.length));
    Module._free(wa);
    Module._free(wb);
    Module._free(wc);
    Module._free(wd);
  };
  /**
   * @type { (out: Float32Array, a : Float32Array, b : Float32Array, stridea : number, strideb: number , len : number,offs : Float32Array) => void}
   */
  globalThis.float32_mm_add = function (
    out,
    ma,
    mb,
    stridea,
    strideb,
    len,
    offs
  ) {
    var elt_size = out.BYTES_PER_ELEMENT;
    var wa = sendToWasmF32(ma);
    var wb = sendToWasmF32(mb);
    var wd = sendToWasmF32(offs);
    var wc = Module._malloc(out.length * elt_size);
    Module._mm_add_float(wc, wa, wb, stridea, strideb, len, wd);
    out.set(Module.HEAPF32.subarray(wc / elt_size, wc / elt_size + out.length));
    Module._free(wa);
    Module._free(wb);
    Module._free(wc);
    Module._free(wd);
  };


}

exports.polyfill = polyfill;
