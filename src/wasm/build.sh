# emcc -msimd128 -O3 -s WASM=1 -s EXPORTED_RUNTIME_METHODS='["cwrap"]' -s EXPORTED_FUNCTIONS='["_malloc","_free"]' calc.c
emcc -msimd128 -s SINGLE_FILE=1 -s MODULARIZE=1 -O3 -s WASM=1 -s  -s EXPORTED_FUNCTIONS='["_malloc","_free"]' calc.c
# SINGLE_FILE=1 
# emcc -O3 -s WASM=1 -s EXPORTED_FUNCTIONS='["cwrap","_malloc"]' calc.c

# wasm2wat --enable *.wasm > a.wat