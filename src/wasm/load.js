//@ts-check
var fs = require("fs");
var code = fs.readFileSync("./a.out.wasm");
(async () => {
  const module = await WebAssembly.compile(code);
  const instance = await WebAssembly.instantiate(module,{a:{a:(size)=>{ throw "ou tof memory"}}});
  const result = instance.exports
  console.log(result);
})();
