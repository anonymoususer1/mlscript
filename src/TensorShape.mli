



type t = (* private*) int array


val size : ?offs:int -> t -> int

val d1 : int -> t 

val d2 : int -> int -> t 

val d3 : int -> int -> int -> t 

val d4 : int -> int -> int -> int -> t 

val dims : t -> int 


(** change the first dimension *)
val replace1D : t -> int -> t  

val replaceID :
  t -> int -> int -> t 
  
val removeID : 
  t -> int -> t 

val eq : t -> t -> bool

val calc_stride :
  t -> int array

val calc_offset : 
  t -> int array -> int   

val to_dimension : 
  t -> int -> t   

type result = 
  [
    | `a_has_b_prefix of int
    | `a_has_b_suffix of int 
    | `b_has_a_prefix of int
    | `b_has_a_suffix of int
    | `mismatch
    | `same of t ]  

val cat :   
  t -> t -> result 

val classify:
  t -> t -> 
  result