module B = UI8Binding

type elt = B.elt
let %private { 
  unsafe_get = (.![]);
  unsafe_set = (.![]<-)
} = (module Array)

type t = {
  shape : TensorShape.t;
  data : B.t
  (* The js B.t keeps track of
     such properties which may be useful
     - BYTES_PER_ELEMENT
     - get buffer 
     - get byteLength // the whole size of the data
     - get byteOffset
     - get length // track the number of elements for you
  *)
}
(**
   Invariants:
   [size_of_shape shape] = length (data)
*)


(** return the size of elements *)
let size ({data} : t ) = B.length data
let shape (x : t ) = x.shape
let toArray (x : t) = 
  let size = size x in 
  let output = Array.make size 0 in 
  for i = 0  to size - 1 do 
    output.![i] <- B.unsafe_get x.data i   
  done ; 
  output  

let zeros (shape : TensorShape.t) = 
  let size = TensorShape.size shape in 
  let data = B.make size in 
  { shape ; data}  




let dims (x : t) = x.shape |. TensorShape.dims

let get (x : t ) (ind : int array) : elt = 
  assert (Array.length ind = dims x );
  let ind = (TensorShape.calc_offset x.shape ind) in 
  B.unsafe_get x.data ind

let set (x : t ) (ind : int array) (v : elt) = 
  assert (Array.length ind = dims x );
  let ind = (TensorShape.calc_offset x.shape ind) in 
  B.unsafe_set x.data ind v 


(* output share the same dimension as input *)
let subarr ({shape} as x : t ) offset len : t  =
  let dim = TensorShape.dims shape in 
  assert (dim > 0);
  let first_1d = shape.![0] in 
  assert (offset >=0 && len > 0 && offset + len <= first_1d);
  let new_shape = TensorShape.replace1D shape len in 
  let s_size = TensorShape.size ~offs:1 shape in 
  let offset = s_size * offset in 
  let next = offset + s_size * len in 
  let new_view = x.data |. B.subarray ~beg:offset ~end_:next in  
  {shape = new_shape; data = new_view}  


(** Extract a sub-array of lower dimension from the given big array
     by fixing one or several of the first (left-most) coordinates.
     [Genarray.slice_left a [|i1; ... ; iM|]] returns the 'slice'
     of [a] obtained by setting the first [M] coordinates to
     [i1], ..., [iM].  If [a] has [N] dimensions, the slice has
     dimension [N - M], and the element at coordinates
     [[|j1; ...; j(N-M)|]] in the slice is identical to the element
     at coordinates [[|i1; ...; iM; j1; ...; j(N-M)|]] in the original
     array [a].  No copying of elements is involved: the slice and
     the original array share the same storage space.
*)
let slice ({shape} as x : t) (index : int array) : t = 
  let dim = Array.length shape in 
  let sub_len = Array.length index in 
  assert (sub_len > 0 && sub_len < dim);
  let offset = TensorShape.calc_offset  shape index in 
  (* invariant is checked in calc_offset *)
  (* for i = 0 to sub_len - 1 do 
     assert (shape.![i] > index.![i])  
     done;    *)
  let new_shape = Array.sub shape sub_len (dim -sub_len) in 
  let s_size = TensorShape.size new_shape in 

  let new_data = 
    x.data |. B.subarray ~beg:offset ~end_:(offset + s_size) in 
  { shape = new_shape ; data = new_data}  

(** Do the invariant check and return the shared view *)
let reshape (x : t) shape =
  let s = TensorShape.size shape in 
  assert (s = size x);
  {x with shape = shape}

(** doc behavior when number is out of range*)
let from ( data : elt array) : t = 
  {data = B.from data ; shape = TensorShape.d1 (Array.length data) }


 let fromBase64Aux_browser : string -> B.t  = [%raw{|function(base64){
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for(var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
   }|}]


let fromBase64Aux : string ->  B.t = [%raw{|function(base64){
    return  new Uint8Array(Buffer.from(base64,'base64').buffer)
  }|}]

let fromBase64 s = 
  let fromBase64Aux =   
    match [%external process] with 
    | None -> fromBase64Aux_browser  
    | Some _ -> fromBase64Aux in  
  let data = fromBase64Aux s in 
  {data ; shape = TensorShape.d1 (B.length data)}

let forall  ({data} as x : t) (f : elt -> bool [@bs]) = 
  let yes = ref true  in 
  let i = ref 0 in 
  let s = size x in 
  while !yes && !i < s do 
    if f (B.unsafe_get data !i ) [@bs] then incr i 
    else yes := false 
  done;
  !yes  


let one_shot ({data = x_data } as data : t) (categories : int ) : t =  
  assert (dims data = 1);   
  let s = size data in 
  let new_size = s * categories in 
  let new_data = B.make new_size in   
  let cur = ref 0 in 
  for i = 0 to s - 1 do 
    B.unsafe_set new_data 
      (!cur + B.unsafe_get x_data i ) 1 ;
    cur := !cur + categories;    
  done;
  {shape = [|s; categories|]; data = new_data}   

