var fs = require("fs");

var train_labels = fs
  .readFileSync(`${__dirname}/../dataset/train-labels-idx1-ubyte`)
  .toString("base64");

var train_images = fs
  .readFileSync(`${__dirname}/../dataset/train-images-idx3-ubyte`)
  .toString("base64");

var test_labels = fs
  .readFileSync(`${__dirname}/../dataset/t10k-labels-idx1-ubyte`)
  .toString("base64");

var test_images = fs
  .readFileSync(`${__dirname}/../dataset/t10k-images-idx3-ubyte`)
  .toString("base64");

function byteToString(b) {
  return b;
}

// [train_labels, train_images, test_labels, test_images].map((x) =>
//   byteToString(x)
// );
fs.writeFileSync(
  `${__dirname}//uint8_data.ml`,
  `
let train_labels = UI8.fromBase64 "${byteToString(train_labels)}"
let train_images = UI8.fromBase64 "${byteToString(train_images)}"
let test_labels = UI8.fromBase64  "${byteToString(test_labels)}"
let test_images = UI8.fromBase64  "${byteToString(test_images)}"
`
);
