
type floatarray = float array


type t = {
  shape : int array ;
  stride : int array ; 
  data : floatarray (* chhange *)
}
let %private {
  calc_index_no_check;
  stride_of_shape;
  size_of_shape;
  calc_index_and_check;
} = 
  (module Utils)
(* #if false then *)
(** we can swap this on/off for array bounds checking *)
let%private {unsafe_get = (.![]); unsafe_set = (.![]<-)} = (module Array)
(* #else 
   let {get = (.![]); set = (.![]<-)} = (module Array)
   #end *)
let%private {push } = (module Js.Array2)


let size (x : t ) = size_of_shape x.shape  
let [@inline] ndim (x : t) = Array.length x.shape

let ones shape = 
  let len = size_of_shape shape in 
  let data = Array.make len 1. in 
  let stride = stride_of_shape shape in 
  { shape ; stride ; data}

let zeros shape = 
  let len = size_of_shape shape in 
  let data = Array.make len 0. in 
  let stride = stride_of_shape shape in 
  { shape ; stride ; data}

(** stride is not preserved *)  
let zeros_like {shape} = 
  let len = size_of_shape shape in 
  let data = Array.make len 0. in 
  let stride = stride_of_shape shape in 
  { shape ; stride ; data}

(** [start range) *)  
let arange (start : int) (finish : int) = 
  let len = finish - start in   
  let start = float start in     
  let data = Array.make len 0. in 
  for i = 0 to len - 1 do 
    data.![i] <- start +. float i   
  done;
  {shape = [|len|]; stride = [|1|]; data }   


(** calculate the next avialble index 
    return false if it hit the end
*)  
let next_index ind dims = 
  let num_dims = Array.length ind in 
  let p = ref (num_dims - 1) in 
  let ok = ref false in 
  while !p >= 0 && not !ok do 
    if ind.![!p] +  1 < dims.![!p] then begin 
      ind.![!p] <- ind.![!p] + 1 ;
      ok := true
    end else begin 
      ind.![!p] <- 0; 
      p := !p - 1   
    end done;
  !ok   

(** item_selection.c PyArray_MultiIndexGetItem*)
let get ( {shape ; stride; data} : t) (index : int array) = 
  let cursor = calc_index_and_check index shape stride in
  data.![cursor] 

let set ({shape; stride; data} : t) (index : int array) newval = 
  let cursor = calc_index_and_check index shape stride in    
  data.![cursor] <- newval


let swap ({shape ; stride} as self) (a : int) (b : int) : t =   
  let ndim = Array.length shape in   
  assert ((a < ndim && a >=0) && (b < ndim && b >=0));
  let new_shape, new_stride  = Array.copy shape, Array.copy stride in 
  new_shape.![a] <- shape.![b];
  new_shape.![b] <- shape.![a];
  new_stride.![a] <- stride.![b];
  new_stride.![b] <- stride.![a];
  {self with shape = new_shape; stride = new_stride}

let transpose ({shape; stride} as self) (permute : int array) = 
  let len = Array.length shape in 
  assert (len = Array.length permute) ; 
  let reverse_permutation = Array.make len (-1) in 
  for i = 0 to len - 1 do 
    let axis = permute.![i] in   
    assert ((axis > 0 && axis < len) && reverse_permutation.![axis] = -1);
    reverse_permutation.![axis] <- i
  done ;
  let new_shape, new_stride = Array.copy shape, Array.copy stride in   
  for i = 0 to len - 1 do 
    let axis = permute.![i] in 
    new_stride.![i]<- stride.![axis];
    new_shape.![i] <- shape.![axis]
  done;
  {self with shape = new_shape; stride = new_stride}   

let atomic_can_share_exn shape stride (newdims : int array) = 
  let olddims = [||] in 
  let oldstrides = [||] in
  let newnd = Array.length newdims in 
  let newstrides = Array.make newnd 1 in 
  for oi = 0 to Array.length shape - 1 do 
    let k = shape.![oi] in   
    if k != 1 then begin 
      push olddims k |. ignore ;
      push oldstrides stride.![oi] |. ignore
    end
  done ;
  let oldnd = Array.length olddims in  
  let oi, oj, ni, nj = ref 0, ref 1, ref 0, ref 1 in 

  while !ni < newnd && !oi < oldnd do 
    let np = ref newdims.![!ni] in   
    let op = ref olddims.![!oi] in 
    while !np <> !op do 
      if !np < !op then begin
        np := !np * newdims.![!nj];
        incr nj; 
      end else begin   
        op := !op * olddims.![!oj];
        incr oj
      end 
    done  ;
    for ok = !oi to !oj - 2 do 
      assert (oldstrides.![ok] = olddims.![ok+1 ] * oldstrides.![ok + 1])  
    done ;  
    newstrides.![!nj - 1] <- oldstrides.![!oj - 1];
    for nk = !nj - 1 downto !ni + 1 do 
      newstrides.![nk - 1] <- newstrides.![nk] * newdims.![nk]  
    done ;
    ni := !nj ; 
    incr nj; 
    oi := !oj ; 
    incr oj
  done ;
  newstrides
let view ({shape;stride} as self) (newdims : int array) =     
  assert (size_of_shape shape = size_of_shape newdims);  
  let newstrides = atomic_can_share_exn shape stride newdims in 
  {self with shape = newdims; stride = newstrides}

let reshape ({shape ; stride } as self) (newdims : int array) = 
  let ndim = size_of_shape shape in 
  assert (ndim = size_of_shape newdims);    
  match atomic_can_share_exn shape stride newdims with 
  | newstrides -> 
    {self with shape = newdims; stride = newstrides}
  | exception _ -> (* FIXME more specific excpetion *)
    let out = zeros newdims in 
    let iterind = Array.make ndim 0 in 
    let todo = ref true in 
    let newind = ref 0 in 
    while !todo do 
      let oldind = calc_index_no_check iterind ~stride in       
      out.data.![!newind]<-self.data.![oldind];
      todo := next_index iterind shape;
      incr newind;
    done  ;
    out

(* let sum ( x : t ) =  *)

(** slow version of map *)  
(* let test_map (x : t) (fn : float -> float [@bs]) = 
   let s = size x in 
   let result = zeros_like x in 
   for i = 0 to s - 1 do 
    set result (fn (get x i) [@bs])  
   done    *)


let u = ones [|2;3;5;2|]  
let () = Js.log u 
(* let u1 = (arange 0 24) |. view ([|3;2;3|]) *)
let u2 = (arange 0 24) |. view ([|3;2;1;1;4;1|])



let () = Js.log u2