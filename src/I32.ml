module B = I32Binding

type elt = B.elt
let %private { 
  (* unsafe_get = (.![]); *)
  unsafe_set = (.![]<-)
} = (module Array)

type t = {
  shape : TensorShape.t;
  data : B.t
  (* The js B.t keeps track of
     such properties which may be useful
     - BYTES_PER_ELEMENT
     - get buffer 
     - get byteLength // the whole size of the data
     - get byteOffset
     - get length // track the number of elements for you
  *)
}
(**
   Invariants:
   [size_of_shape shape] = length (data)
*)


(** return the size of elements *)
let size ({data} : t ) = B.length data
let shape (x : t ) = x.shape
let toArray (x : t) = 
  let size = size x in 
  let output = Array.make size 0 in 
  for i = 0  to size - 1 do 
    output.![i] <- B.unsafe_get x.data i   
  done ; 
  output  

let zeros (shape : TensorShape.t) = 
  let size = TensorShape.size shape in 
  let data = B.make size in 
  { shape ; data}  
  
(* let forEach (u : t) =     *)