(* let p = 
   match [%node __dirname] with  
   | Some x -> x 
   | None -> assert false
   module P = Node.Path *)
(* let label_tensor2 : F.t =  
   UI8.loadMnist (P.join [|p;  "..";"dataset"; "train-labels-idx1-ubyte"|])
   |. UI8.one_shot 10 
   |. F.ofUI8 *)
(* let train_tensor : F.t  =
   UI8.loadMnistImage  (P.join [|p; "..";"dataset"; "train-images-idx3-ubyte"|])
   |. UI8.reshape [|60_000; 784|]
   |. F.ofUI8
   |. F.map (fun [@bs] x -> x /. 255.)
   ;; *)
module F64 = struct 
  module F = F64  
  let label_tensor : F.t = 
    let raw = Uint8_data.train_labels in 
    raw 
    |. UI8.subarr 8 (UI8.size raw - 8)
    |. UI8.one_shot 10 
    |. F.ofUI8 

  let train_tensor = 
    let raw = Uint8_data.train_images in 
    raw 
    |. UI8.subarr 16 (UI8.size raw - 16)
    |. UI8.reshape [|60_000; 784|]
    |. F.ofUI8
    |. F.map (fun [@bs] x -> x /. 255.)

  type params = { 
    w1 : float array array; [@as "W1"]
    w2 : float array array [@as "W2"]
  }

  (* external data : params = "../dataset/params.json" [@@module]
     let {w1; w2 } = data  *)

  (* let w1 = F.from2D w1 |. F.reshape [|784;50|]
     let w2 = F.from2D w2 |. F.reshape [|50;10|] *)

  let w1 = F.randn [|784; 50|] |. F.map_ (fun [@bs] x -> x *. 0.1)
  let w2 = F.randn [|50; 10|] |. F.map_ (fun [@bs] x -> x *. 0.1)
  let b1 = 
    F.zeros [|50|]
  let b2 = 
    F.zeros [|10|] 
end

module F32 = struct 
  module F = F32  
  let label_tensor : F.t = 
    let raw = Uint8_data.train_labels in 
    raw 
    |. UI8.subarr 8 (UI8.size raw - 8)
    |. UI8.one_shot 10 
    |. F.ofUI8 

  let train_tensor = 
    let raw = Uint8_data.train_images in 
    raw 
    |. UI8.subarr 16 (UI8.size raw - 16)
    |. UI8.reshape [|60_000; 784|]
    |. F.ofUI8
    |. F.map (fun [@bs] x -> x /. 255.)

  type params = { 
    w1 : float array array; [@as "W1"]
    w2 : float array array [@as "W2"]
  }

  (* external data : params = "../dataset/params.json" [@@module]
     let {w1; w2 } = data  *)

  (* let w1 = F.from2D w1 |. F.reshape [|784;50|]
     let w2 = F.from2D w2 |. F.reshape [|50;10|] *)

  let w1 = F.randn [|784; 50|] |. F.map_ (fun [@bs] x -> x *. 0.1)
  let w2 = F.randn [|50; 10|] |. F.map_ (fun [@bs] x -> x *. 0.1)
  let b1 = 
    F.zeros [|50|]
  let b2 = 
    F.zeros [|10|] 
end