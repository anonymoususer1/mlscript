
[@@@warning"-44"]


let {label_tensor ; train_tensor; w1; w2} = 
  (module Data_provider.F64)


let weight_init_std = 0.01
(* let w1 = 
   F64.(randn [|784; 50|] |. map (fun [@bs] x ->  weight_init_std *. x )) *)
let w1' = F64.zeros w1.shape 
let b1 = 
  F64.zeros [|50|]
let b1' = F64.zeros b1.shape
(* let w2 = 
   F64.(randn [|50; 10|] |. map (fun [@bs] x -> weight_init_std *. x )) *)
let w2' = F64.zeros w2.shape
let b2 = 
  F64.zeros [|10|]    
let b2' = F64.zeros b2.shape   
;;

let batch_size = 100. 
let predict (input : F64.t) (t : F64.t)  : float =
  let open F64 in  
  let z1 = (input  @ w1) + b1 in 
  let y1 = sigmoid z1 in 
  let z2 = (y1 @ w2) + b2 in
  let y = softmax z2 in   
  let loss =  
    (- 1. /. batch_size) *. sum_all (t * log y )  in 
  (* Js.log loss; *)
  loss

let loss = 
  (predict 
     (F64.subarr train_tensor 0 100)  
     (F64.subarr label_tensor 0 100)
  )

;; Js.log loss


(* let b2_num =    
   Grad.numeric_gradient 
    (fun _ -> predict 
        (F64.subarr label_tensor 0 (int_of_float batch_size))
        (F64.subarr train_tensor 0 (int_of_float batch_size)))
    b2 

   let w2_num =    
   Grad.numeric_gradient 
    (fun _ -> predict 
        (F64.subarr label_tensor 0 (int_of_float batch_size))
        (F64.subarr train_tensor 0 (int_of_float batch_size)))
    w2 
   let w1_num =    
   Grad.numeric_gradient 
    (fun _ -> predict 
        (F64.subarr label_tensor 0 (int_of_float batch_size))
        (F64.subarr train_tensor 0 (int_of_float batch_size)))
    w1 

   let b1_num =    
   Grad.numeric_gradient 
    (fun _ -> predict 
        (F64.subarr label_tensor 0 (int_of_float batch_size))
        (F64.subarr train_tensor 0 (int_of_float batch_size)))
    b1 *)

let train (train_tensor : F64.t) (label_tensor : F64.t)  = 
  let losses = [||] in 
  let total_size =  train_tensor.shape.(0) in    
  for i = 0 to total_size / 100 - 1 do 
    let input = F64.subarr train_tensor (i * 100)  100 in 
    let t =  F64.subarr label_tensor (i * 100) 100 in  
    let open F64 in  
    let z1 = (input @ w1) + b1 in 
    let y1 = sigmoid z1 in 
    let z2 = (y1 @ w2 ) + b2 in
    let y = softmax z2 in   
    let loss = (- 1. /. batch_size) *. sum_all (t * log y ) in 
    Js.Array2.push losses loss |. ignore ; 
    let z2' = (y - t) |.map_ (fun [@bs] x ->  x /. batch_size)  in 
    (* w2', b2', w1', b1' *)
    let w2' = transpose y1 @ z2' in 
    let b2' = sum z2' ~axis:0 ~keepDims:false in     
    let y1' = z2' @ transpose w2 in 
    let z1' = y1' * (y1 |. map (fun [@bs] x-> (1.-. x ) *. x ))  in 
    let w1' = transpose input @ z1' in 
    let b1' =  sum z1' ~axis:0 ~keepDims:false in 
    update_with_derivative w1 w1'; 
    update_with_derivative w2 w2';
    update_with_derivative b1 b1';
    update_with_derivative b2 b2';
  done ;
  losses

;;


let batch_size = 100
let learning_rate = 0.1
let train_size = train_tensor.shape.(0)

;; let r = train train_tensor label_tensor
;; Js.log r 

