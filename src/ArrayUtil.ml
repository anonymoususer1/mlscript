


type 'a t = 'a array


(* let shuffle ( x : 'a t) = 
  let ci = ref (Array.length x) in 
  while !ci <> 0 do 
    let random = RandomUtil.randomInt !ci in 
    decr ci;
    
  done   *)


(* let h = Belt.Array.shuffleInPlace   *)