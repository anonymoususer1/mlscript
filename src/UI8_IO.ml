

(** data format is described in http://yann.lecun.com/exdb/mnist/ : *)  
let loadMnist filename : UI8.t = 
  let data,length = ([%raw{|function(filename){
    var fs = require('fs')
    var data = fs.readFileSync(filename).slice(8)
    return [new globalThis.Uint8Array(data.buffer,data.byteOffset,data.byteLength),data.length] 
  }|}]) filename in 
  {shape = [|length|]; data }  

let loadMnistImage filename : UI8.t =   
  let data,length = ([%raw{|function(filename){
    var fs = require('fs')
    var data = fs.readFileSync(filename).slice(16)
    return [new globalThis.Uint8Array(data.buffer,data.byteOffset,data.byteLength),data.length] 
  }|}]) filename in 
  {shape = [|length|]; data }  
