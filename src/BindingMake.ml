module Make ( S : sig
    type typedarray
    type elt   
  end) = struct
  open S  
  external length : typedarray -> int = "length" [@@get]
  external slice : typedarray -> typedarray = "slice" [@@send]

  external set : typedarray -> typedarray -> offset:int -> unit = "set" [@@send] 
  (** set, slice both do the copy while slice create a new one
  *)


  external map : typedarray -> (elt -> elt [@uncurry]) -> typedarray = "map" [@@send]

  external fill : typedarray -> elt -> unit = "fill" [@@send]  
  (* it returns a view *)  
  
  (* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/subarray
  [begin,end)
  *)
  external subarray : typedarray -> beg:int -> end_:int -> typedarray = "subarray"
  [@@send]

  external unsafe_get : typedarray -> int -> elt = "" 
  [@@get_index]

  external unsafe_set : typedarray -> int -> elt -> unit = ""
  [@@set_index]
end