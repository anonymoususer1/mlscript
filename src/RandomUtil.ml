

(**
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
   generates value `[0,1)`
*)
(* external random : 
  unit -> float = "random" 
[@@val]
[@@scope "Math"] *)


external id :  int32 -> int = "%identity"
external id1 :  int -> int32 = "%identity"
(**
   [lo,hi)
*)  
let randint (lo : int) (hi : int) : int  = 
  lo + id (Random.int32 (id1 hi : int32)) 


(* uniform distribution  [0., 1,]*)  
let rec rand () = 
  let h = Random.float 1. in (* inclusive *)
  if h = 1. then rand ()
  else h 

(** return values between (0.,1.) *)  
let rec rand_aux () = 
  let h = Random.float 1. in 
  if h = 1. || h = 0. then rand_aux ()
  else h 

external pi : float = "PI" 
[@@val] [@@scope "Math"]

let randn () =    
  let u = rand_aux () in 
  let v = rand_aux () in 
  sqrt (-2.0 *. log u) *. cos (2.0 *. pi *. v)