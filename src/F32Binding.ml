type t

type elt = float 

external from : elt array -> t = "Float32Array" [@@new]
external make : int -> t = "Float32Array" [@@new]

let zero : elt = 0.

include (BindingMake.Make(struct 
  type nonrec elt = elt 
  type nonrec typedarray = t
end))