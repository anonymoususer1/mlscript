module B  = F64.B

let numeric_gradient_no_batch (f : F64.t -> float) ({data} as x : F64.t) =
  let h = 1e-4 in 
  let {data = grad_data } as grad = F64.zeros x.shape in 
  for i = 0 to F64.size x - 1 do 
    let tmp_val = B.unsafe_get data i in 
    B.unsafe_set data i (tmp_val +. h) ;
    let fxh1 = f x in 
    B.unsafe_set data i  (tmp_val -. h );
    let fxh2 = f x in 
    B.unsafe_set grad_data i  ((fxh1 -. fxh2) /. (2. *. h))
  done  ;
  grad  

  
let numeric_gradient_no_batch_ 
    (f : F64.t -> float) ({data} as x : F64.t)
    ({data = grad_data } : F64.t) =
  let h = 1e-4 in 
  for i = 0 to F64.size x - 1 do 
    let tmp_val = B.unsafe_get data i in 
    B.unsafe_set data i (tmp_val +. h) ;
    let fxh1 = f x in 
    B.unsafe_set data i  (tmp_val -. h );
    let fxh2 = f x in 
    B.unsafe_set grad_data i  ((fxh1 -. fxh2) /. (2. *. h))
  done 


let numeric_gradient (f : F64.t -> float)  (x : F64.t) = 
  if F64.dims x = 1 then 
    numeric_gradient_no_batch (fun x -> f x ) x 
  else 
    let grad = F64.zeros  x.shape in 
    for i = 0 to x.shape.(0) - 1 do 
      numeric_gradient_no_batch_ 
        (fun x -> f x  )  (F64.slice x [|i|]) (F64.slice grad [|i|]) 
    done  ;
    grad
