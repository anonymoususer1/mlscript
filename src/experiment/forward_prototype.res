type df = {
  mutable p: float,
  mutable t: float,
}

let primal = df => df.p
let tangent = df => df.t

let sin_ad = x => {
  let p = primal(x)
  let t = tangent(x)
  let p' = Math_ops.sin(p)
  let t' = Math_ops.cos(p) *. t
  {p: p', t: t'}
}

let exp_ad = x => {
  let p = primal(x)
  let t = tangent(x)
  let p' = Math_ops.exp(p)
  let t' = p' *. t
  {p: p', t: t'}
}

let mul_ad = (a, b) => {
  let pa = primal(a)
  let ta = tangent(a)
  let pb = primal(b)
  let tb = tangent(b)
  let p' = pa *. pb
  let t' = pa *. tb +. ta *. pb
  {p: p', t: t'}
}

let add_ad = (a, b) => {
  let pa = primal(a)
  let ta = tangent(a)
  let pb = primal(b)
  let tb = tangent(b)
  let p' = pa +. pb
  let t' = ta +. tb
  {p: p', t: t'}
}

let div_ad = (a, b) => {
  let pa = primal(a)
  let ta = tangent(a)
  let pb = primal(b)
  let tb = tangent(b)
  let p' = pa /. pb
  let t' = (ta *. pb -. tb *. pa) /. (pb *. pb)
  {p: p', t: t'}
}

let diff = f => {
  let f' = (x, y) => {
    let r = f(x, y)
    (primal(r), tangent(r))
  }
  f'
}

let x0 = {p: 1., t: 1.}
let x1 = {p: 1., t: 0.}

let f = (x0, x1) => {
  let v2 = sin_ad(x0)
  let v3 = mul_ad(x0, x1)
  let v4 = add_ad(v2, v3)
  let v5 = {p: 1., t: 0.}
  let v6 = exp_ad(v4)
  let v7 = {p: 1., t: 0.}
  let v8 = add_ad(v5, v6)
  let v9 = div_ad(v7, v8)
  v9
}

let (pri, tan) = diff(f, x0, x1)
