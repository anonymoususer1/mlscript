type rec dr = {
  mutable p: float,
  mutable a: float,
  mutable adj_fun: (float, list<(float, dr)>) => list<(float, dr)>,
}

let primal = dr => dr.p
let adjoint = dr => dr.a
let adj_fun = dr => dr.adj_fun

let rec reverse_push = (xs: list<(float, dr)>) =>
  switch xs {
  | list{} => ()
  | list{(v, dr), ...t} =>
    dr.a = dr.a +. v
    let stack = dr.adj_fun(dr.a, t)
    reverse_push(stack)
  }

let diff = f => {
  let f' = x => {
    let r = f(x)
    reverse_push(list{(1., r)})
    let (x0, x1) = x
    (primal(x0), x0.a, primal(x1), x1.a)
  }

  f'
}

let sin_ad = (dr: dr) => {
  let p = primal(dr)
  let p' = Math_ops.sin(p)
  let adjfun' = (ca, t) => {
    let r = ca *. Math_ops.cos(p)
    list{(r, dr), ...t}
  }

  {p: p', a: 0., adj_fun: adjfun'}
}

let mul_ad = (dr1: dr, dr2: dr) => {
  let p1 = primal(dr1)
  let p2 = primal(dr2)
  let p' = Math_ops.mul(p1, p2)
  let adjfun' = (ca, t) => {
    let r1 = ca *. p2
    let r2 = ca *. p1
    list{(r1, dr1), (r2, dr2), ...t}
  }

  {p: p', a: 0., adj_fun: adjfun'}
}

let make_reverse = (v: float): dr => {
  let a = 0.
  let adj_fun = (_a, t) => t
  {p: v, a: a, adj_fun: adj_fun}
}

let exp_ad = dr => {
  let p = primal(dr)
  let p' = Math_ops.exp(p)
  let adjfun' = (ca, t) => {
    let r = ca *. Math_ops.exp(p)
    list{(r, dr), ...t}
  }

  {p: p', a: 0., adj_fun: adjfun'}
}

let add_ad = (dr1, dr2) => {
  let p1 = primal(dr1)
  let p2 = primal(dr2)
  let p' = Math_ops.add(p1, p2)
  let adjfun' = (ca, t) => {
    let r1 = ca
    let r2 = ca
    list{(r1, dr1), (r2, dr2), ...t}
  }

  {p: p', a: 0., adj_fun: adjfun'}
}

let div_ad = (dr1, dr2) => {
  let p1 = primal(dr1)
  let p2 = primal(dr2)
  let p' = Math_ops.div(p1, p2)
  let adjfun' = (ca, t) => {
    let r1 = ca /. p2
    let r2 = ca *. -.p1 /. (p2 *. p2)
    list{(r1, dr1), (r2, dr2), ...t}
  }
  {p: p', a: 0., adj_fun: adjfun'}
}

let x1 = make_reverse(1.)
let x0 = make_reverse(1.)

let f = x => {
  let (x0, x1) = x
  let v2 = sin_ad(x0)
  let v3 = mul_ad(x0, x1)
  let v4 = add_ad(v2, v3)
  let v5 = make_reverse(1.)
  let v6 = exp_ad(v4)
  let v7 = make_reverse(1.)
  let v8 = add_ad(v5, v6)
  let v9 = div_ad(v7, v8)
  v9
}

let (pri_x0, adj_x, pri_x1, adj_x1) = diff(f, (x0, x1))
