
(** This file is adapted from the ocaml stdlib
  It is not used here, for references
 *)
type float32_elt = Float32_elt
type float64_elt = Float64_elt

type ('a, 'b) kind = 
    Float32 : (float, float32_elt) kind
  | Float64 : (float, float64_elt) kind

(* Keep those constants in sync with the caml_ba_kind enumeration
   in bigarray.h *)

let float32 = Float32
let float64 = Float64


let kind_size_in_bytes : type a b. (a, b) kind -> int = function
  | Float32 -> 4
  | Float64 -> 8

type ('a,'b) typedarray  

type ('a, 'b) t = {
  shape : int array;
  kind : ('a,'b) kind; (* redundant*)
  data : ('a,'b) typedarray
}

external make_float32 : int -> (float,float32_elt) typedarray = "Float32Array" [@@new]
external make_float64 : int -> (float,float64_elt) typedarray = "Float64Array" [@@new]
external typedarray_set : 
  ('a,'b)typedarray -> 
  ('a,'b)typedarray -> 
  unit = "set" [@@send]

external typedarray_fill : 
  ('a,'b) typedarray -> 
  'a -> unit = "fill" [@@send]  
external (.!()) : ('a,'b) typedarray -> int -> 'a = "" [@@get_index]
external (.!()<-) : ('a,'b) typedarray -> int -> 'a -> unit = "" [@@set_index]

let%private  { unsafe_get = (.![]); unsafe_set = (.![]<-)} = (module Array)

(** [) *)
external subarray : 
  ('a,'b) typedarray -> int -> int -> ('a,'b) typedarray = "subarray" [@@send]

let {size_of_shape ;
     calc_index_and_check;
     stride_of_shape;
     eq_shape;
    } = (module Utils)
module Genarray = struct
  type nonrec ('a,'b) t = ('a, 'b) t
  let create (type f elt) (kind : (f,elt) kind) shape  : (f,elt) t=
    let size = size_of_shape shape in 
    let data : (f, elt) typedarray= 
      match kind with 
      | Float32 -> make_float32 size 
      | Float64 -> make_float64 size in 
    {shape ; data;kind}

  let get ({shape; data}: ('a, 'b) t) (ind: int array) : 'a
    = 
    assert (Array.length ind = Array.length shape);
    let stride = stride_of_shape shape in 
    let ind = calc_index_and_check ind shape stride in 
    data.!(ind)

  let set ({shape ; data} : ('a, 'b) t)  (ind : int array)  ( v : 'a )
    = 
    let stride = stride_of_shape shape in 
    let ind = calc_index_and_check ind shape stride in 
    data.!(ind) <- v

  let num_dims: ('a, 'b) t -> int = fun x -> Array.length x.shape

  let nth_dim: ('a, 'b) t -> int -> int = fun x i -> 
    x.shape.(i)

  let dims a =
    let n = num_dims a in
    let d = Array.make n 0 in
    for i = 0 to n-1 do d.(i) <- nth_dim a i done;
    d

  let kind: ('a, 'b) t -> ('a, 'b) kind = fun x -> x.kind

  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (Array.fold_left ( * ) 1 (dims arr))


  (* output share the same dimension as input *)    
  let sub_left: ('a,'b) t -> int -> int -> ('a,'b) t
    = fun ({shape;data} as self) offset len -> 
      let dim = Array.length shape in 
      assert (dim > 0);
      let first_1d = shape.![0] in 
      assert (offset >=0 && len > 0 && offset + len <= first_1d);
      let new_shape = Array.copy shape in 
      new_shape.![0] <- len ;
      let s_size = size_of_shape ~offs:1 shape in
      let offset = s_size * offset in 
      let next = s_size * (offset + len) in 
      let new_data = subarray data offset next in 
      (* exclusive *)
      {self with shape = new_shape; data = new_data;}


  let slice_left: ('a,'b) t -> int array ->
    ('a,'b) t
    = fun ({shape; data} as self) index -> 
      let dim = Array.length shape in 
      let sub_len = Array.length index in 
      assert (sub_len > 0 && sub_len < dim);
      let new_shape = Array.sub shape sub_len (dim - sub_len) in 
      let s_size =  size_of_shape ~offs:(sub_len) shape in 
      let offset = size_of_shape index * s_size in      
      let new_data = 
        subarray data offset (offset + s_size) in 
      {self with shape = new_shape; data = new_data}

  (* types guarantee that storage is the same *)      
  let blit: ('a, 'b) t -> ('a, 'b) t -> unit
    = fun src dst -> 
      assert (eq_shape dst.shape src.shape);
      typedarray_set dst.data src.data

  let fill: ('a, 'b) t -> 'a -> unit = fun self v -> 
    typedarray_fill self.data v 
end


module Array1 = struct
  type ('a, 'b) t =('a, 'b) Genarray.t
  let create kind  dim =
    Genarray.create kind  [|dim|]
  let get: ('a, 'b) t -> int -> 'a = fun self i -> 
    Genarray.get self [|i|]
  let set: ('a, 'b) t -> int -> 'a -> unit = fun self i v -> 
    Genarray.set self [|i|] v 
  (*FIXME*)  
  let unsafe_get: ('a, 'b) t -> int -> 'a =
    get
  (*FIXME*) 
  let unsafe_set: ('a, 'b) t -> int -> 'a -> unit
    =  set
  let dim: ('a, 'b) t -> int = fun self -> 
    self.shape.(0)
  let kind: ('a, 'b) t -> ('a, 'b) kind = Genarray.kind

  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (dim arr)

  let sub: ('a, 'b) t -> int -> int -> ('a, 'b) t = Genarray.sub_left

  let blit: ('a, 'b) t -> ('a, 'b) t -> unit = Genarray.blit
  let fill: ('a, 'b) t -> 'a -> unit = Genarray.fill
  let of_array kind  data =
    let ba = create kind  (Array.length data) in
    for i = 0 to Array.length data - 1 do 
      unsafe_set ba i data.(i) 
    done;
    ba
end

module Array2 = struct
  type ('a, 'b) t =('a, 'b) Genarray.t
  let create kind  dim1 dim2 =
    Genarray.create kind  [|dim1; dim2|]
  let get: ('a, 'b) t -> int -> int -> 'a = 
    fun self i j -> Genarray.get self [|i;j|]
  let set: ('a, 'b) t -> int -> int -> 'a -> unit = 
    fun self i j v -> Genarray.set self [|i;j|] v   
  let unsafe_get: ('a, 'b) t -> int -> int -> 'a
    = get
  let unsafe_set: ('a, 'b) t -> int -> int -> 'a -> unit
    = set
  let dim1: ('a, 'b) t -> int = fun self -> self.shape.(0)
  let dim2: ('a, 'b) t -> int = fun self -> self.shape.(1)
  let kind: ('a, 'b) t -> ('a, 'b) kind = Genarray.kind


  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (dim1 arr) * (dim2 arr)

  let sub_left: ('a,'b) t -> int -> int -> ('a,'b) t
    = Genarray.sub_left 
  let slice_left a n = Genarray.slice_left a [|n|]
  
  let blit: ('a, 'b) t -> ('a, 'b) t -> unit = Genarray.blit
  let fill: ('a, 'b) t -> 'a -> unit = Genarray.fill
  let of_array kind  data =
    let dim1 = Array.length data in
    let dim2 = if dim1 = 0 then 0 else Array.length data.(0) in
    let ba = create kind  dim1 dim2 in
    for i = 0 to dim1 - 1 do
      let row = data.(i) in
      if Array.length row <> dim2 then
        invalid_arg("Bigarray.Array2.of_array: non-rectangular data");
      for j = 0 to dim2 - 1 do
        unsafe_set ba i  0 row.(j)
      done
    done;
    ba
  
end

module Array3 = struct
  type ('a, 'b) t =('a, 'b) Genarray.t
  let create kind  dim1 dim2 dim3 =
    Genarray.create kind  [|dim1; dim2; dim3|]
  let get: ('a, 'b) t -> int -> int -> int -> 'a = 
      fun self i j k -> Genarray.get self [|i;j;k|]
  let set: ('a, 'b) t -> int -> int -> int -> 'a -> unit
    = fun self i j k v -> Genarray.set self [|i;j;k|] v 
  let unsafe_get: ('a, 'b) t -> int -> int -> int -> 'a
    = get
  let unsafe_set: ('a, 'b) t -> int -> int -> int -> 'a -> unit
    = set
  let dim1: ('a, 'b) t -> int = fun self -> self.shape.(0)
  let dim2: ('a, 'b) t -> int = fun self -> self.shape.(1)
  let dim3: ('a, 'b) t -> int = fun self -> self.shape.(2)
  let kind: ('a, 'b) t -> ('a, 'b) kind = Genarray.kind
  
  let size_in_bytes arr =
    (kind_size_in_bytes (kind arr)) * (dim1 arr) * (dim2 arr) * (dim3 arr)

  let sub_left: ('a,'b) t -> int -> int -> ('a,'b) t
    = Genarray.sub_left
  
  let slice_left_1 a n m = Genarray.slice_left a [|n; m|]

  let slice_left_2 a n = Genarray.slice_left a [|n|]

  let blit: ('a, 'b) t -> ('a, 'b) t -> unit = Genarray.blit
  let fill: ('a, 'b) t -> 'a -> unit = Genarray.fill
  let of_array  kind  data =
    let dim1 = Array.length data in
    let dim2 = if dim1 = 0 then 0 else Array.length data.(0) in
    let dim3 = if dim2 = 0 then 0 else Array.length data.(0).(0) in
    let ba = create kind dim1 dim2 dim3 in    
    for i = 0 to dim1 - 1 do
      let row = data.(i) in
      if Array.length row <> dim2 then
        invalid_arg("Bigarray.Array3.of_array: non-cubic data");
      for j = 0 to dim2 - 1 do
        let col = row.(j) in
        if Array.length col <> dim3 then
          invalid_arg("Bigarray.Array3.of_array: non-cubic data");
        for k = 0 to dim3 - 1 do
          unsafe_set ba (i ) (j ) (k ) col.(k)
        done
      done
    done;
    ba
end


external genarray_of_array1: ('a, 'b) Array1.t ->('a, 'b) Genarray.t
  = "%identity"
external genarray_of_array2: ('a, 'b) Array2.t ->('a, 'b) Genarray.t
  = "%identity"
external genarray_of_array3: ('a, 'b) Array3.t ->('a, 'b) Genarray.t
  = "%identity"

let array1_of_genarray a =
  if Genarray.num_dims a = 1 then a
  else invalid_arg "Bigarray.array1_of_genarray"
let array2_of_genarray a =
  if Genarray.num_dims a = 2 then a
  else invalid_arg "Bigarray.array2_of_genarray"
let array3_of_genarray a =
  if Genarray.num_dims a = 3 then a
  else invalid_arg "Bigarray.array3_of_genarray"

let reshape:
  ('a, 'b) Genarray.t -> int array ->('a, 'b) Genarray.t
  = fun self shape ->
  assert (size_of_shape self.shape = size_of_shape shape);  
  {self with shape }

let reshape_1 a dim1 = reshape a [|dim1|]
let reshape_2 a dim1 dim2 = reshape a [|dim1;dim2|]
let reshape_3 a dim1 dim2 dim3 = reshape a [|dim1;dim2;dim3|]

