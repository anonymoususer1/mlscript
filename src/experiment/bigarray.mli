
(** This file is adapted from the ocaml stdlib
  It is not used here, for references
 *)
type float32_elt = Float32_elt
type float64_elt = Float64_elt

type ('a, 'b) kind = 
    Float32 : (float, float32_elt) kind
  | Float64 : (float, float64_elt) kind


val float32 : (float, float32_elt) kind
(** See {!Bigarray.char}. *)

val float64 : (float, float64_elt) kind
(** See {!Bigarray.char}. *)

val kind_size_in_bytes : ('a, 'b) kind -> int
(** [kind_size_in_bytes k] is the number of bytes used to store
   an element of type [k].

   @since 4.03.0 *)

(** {1 Generic arrays (of arbitrarily many dimensions)} *)

module Genarray :
  sig
  type ('a, 'b) t 

  val create: ('a, 'b) kind ->  int array -> ('a, 'b) t
  (** [Genarray.create kind layout dimensions] returns a new big array
     whose element kind is determined by the parameter [kind] (one of
     [float32], [float64], [int8_signed], etc) and whose layout is
     determined by the parameter [layout] (one of [c_layout] or
     [fortran_layout]).  The [dimensions] parameter is an array of
     integers that indicate the size of the big array in each dimension.
     The length of [dimensions] determines the number of dimensions
     of the bigarray.

     For instance, [Genarray.create int32 c_layout [|4;6;8|]]
     returns a fresh big array of 32-bit integers, in C layout,
     having three dimensions, the three dimensions being 4, 6 and 8
     respectively.

     Big arrays returned by [Genarray.create] are not initialized:
     the initial values of array elements is unspecified.

     [Genarray.create] raises [Invalid_argument] if the number of dimensions
     is not in the range 0 to 16 inclusive, or if one of the dimensions
     is negative. *)

  val num_dims: ('a, 'b) t -> int 
  (** Return the number of dimensions of the given big array. *)

  val dims : ('a, 'b) t -> int array
  (** [Genarray.dims a] returns all dimensions of the big array [a],
     as an array of integers of length [Genarray.num_dims a]. *)

  val nth_dim: ('a, 'b) t -> int -> int 
  (** [Genarray.nth_dim a n] returns the [n]-th dimension of the
     big array [a].  The first dimension corresponds to [n = 0];
     the second dimension corresponds to [n = 1]; the last dimension,
     to [n = Genarray.num_dims a - 1].
     Raise [Invalid_argument] if [n] is less than 0 or greater or equal than
     [Genarray.num_dims a]. *)

  val kind: ('a, 'b) t -> ('a, 'b) kind
  (** Return the kind of the given big array. *)

  val size_in_bytes : ('a, 'b) t -> int
  (** [size_in_bytes a] is the number of elements in [a] multiplied
    by [a]'s {!kind_size_in_bytes}.

    @since 4.03.0 *)

  val get: ('a, 'b) t -> int array -> 'a 
  (** Read an element of a generic big array.
     [Genarray.get a [|i1; ...; iN|]] returns the element of [a]
     whose coordinates are [i1] in the first dimension, [i2] in
     the second dimension, ..., [iN] in the [N]-th dimension.

     If [a] has C layout, the coordinates must be greater or equal than 0
     and strictly less than the corresponding dimensions of [a].
     If [a] has Fortran layout, the coordinates must be greater or equal
     than 1 and less or equal than the corresponding dimensions of [a].
     Raise [Invalid_argument] if the array [a] does not have exactly [N]
     dimensions, or if the coordinates are outside the array bounds.

     If [N > 3], alternate syntax is provided: you can write
     [a.{i1, i2, ..., iN}] instead of [Genarray.get a [|i1; ...; iN|]].
     (The syntax [a.{...}] with one, two or three coordinates is
     reserved for accessing one-, two- and three-dimensional arrays
     as described below.) *)

  val set: ('a, 'b) t -> int array -> 'a -> unit
  (** Assign an element of a generic big array.
     [Genarray.set a [|i1; ...; iN|] v] stores the value [v] in the
     element of [a] whose coordinates are [i1] in the first dimension,
     [i2] in the second dimension, ..., [iN] in the [N]-th dimension.

     The array [a] must have exactly [N] dimensions, and all coordinates
     must lie inside the array bounds, as described for [Genarray.get];
     otherwise, [Invalid_argument] is raised.

     If [N > 3], alternate syntax is provided: you can write
     [a.{i1, i2, ..., iN} <- v] instead of
     [Genarray.set a [|i1; ...; iN|] v].
     (The syntax [a.{...} <- v] with one, two or three coordinates is
     reserved for updating one-, two- and three-dimensional arrays
     as described below.) *)

  val sub_left: ('a, 'b) t -> int -> int -> ('a, 'b) t  
  (** Extract a sub-array of the given big array by restricting the
     first (left-most) dimension.  [Genarray.sub_left a ofs len]
     returns a big array with the same number of dimensions as [a],
     and the same dimensions as [a], except the first dimension,
     which corresponds to the interval [[ofs ... ofs + len - 1]]
     of the first dimension of [a].  No copying of elements is
     involved: the sub-array and the original array share the same
     storage space.  In other terms, the element at coordinates
     [[|i1; ...; iN|]] of the sub-array is identical to the
     element at coordinates [[|i1+ofs; ...; iN|]] of the original
     array [a].

     [Genarray.sub_left] applies only to big arrays in C layout.
     Raise [Invalid_argument] if [ofs] and [len] do not designate
     a valid sub-array of [a], that is, if [ofs < 0], or [len < 0],
     or [ofs + len > Genarray.nth_dim a 0]. *)

  val slice_left:
    ('a, 'b) t -> int array -> ('a, 'b) t  
  (** Extract a sub-array of lower dimension from the given big array
     by fixing one or several of the first (left-most) coordinates.
     [Genarray.slice_left a [|i1; ... ; iM|]] returns the 'slice'
     of [a] obtained by setting the first [M] coordinates to
     [i1], ..., [iM].  If [a] has [N] dimensions, the slice has
     dimension [N - M], and the element at coordinates
     [[|j1; ...; j(N-M)|]] in the slice is identical to the element
     at coordinates [[|i1; ...; iM; j1; ...; j(N-M)|]] in the original
     array [a].  No copying of elements is involved: the slice and
     the original array share the same storage space.

     [Genarray.slice_left] applies only to big arrays in C layout.
     Raise [Invalid_argument] if [M >= N], or if [[|i1; ... ; iM|]]
     is outside the bounds of [a]. *)

  val blit: ('a, 'b) t -> ('a, 'b) t -> unit

  (** Copy all elements of a big array in another big array.
     [Genarray.blit src dst] copies all elements of [src] into
     [dst].  Both arrays [src] and [dst] must have the same number of
     dimensions and equal dimensions.  Copying a sub-array of [src]
     to a sub-array of [dst] can be achieved by applying [Genarray.blit]
     to sub-array or slices of [src] and [dst]. *)

  val fill: ('a, 'b) t -> 'a -> unit 
  (** Set all elements of a big array to a given value.
     [Genarray.fill a v] stores the value [v] in all elements of
     the big array [a].  Setting only some elements of [a] to [v]
     can be achieved by applying [Genarray.fill] to a sub-array
     or a slice of [a]. *)


  end

(** {1 Zero-dimensional arrays} *)


(** {1 One-dimensional arrays} *)

(** One-dimensional arrays. The [Array1] structure provides operations
   similar to those of
   {!Bigarray.Genarray}, but specialized to the case of one-dimensional arrays.
   (The {!Array2} and {!Array3} structures below provide operations
   specialized for two- and three-dimensional arrays.)
   Statically knowing the number of dimensions of the array allows
   faster operations, and more precise static type-checking. *)
module Array1 : sig
  type ('a, 'b) t
  (** The type of one-dimensional big arrays whose elements have
     OCaml type ['a], representation kind ['b], and memory layout ['c]. *)

  val create: ('a, 'b) kind  -> int -> ('a, 'b) t

  val dim: ('a, 'b) t -> int 
  (** Return the size (dimension) of the given one-dimensional
     big array. *)

  val kind: ('a, 'b) t -> ('a, 'b) kind 
  (** Return the kind of the given big array. *)

  val size_in_bytes : ('a, 'b) t -> int
  (** [size_in_bytes a] is the number of elements in [a]
    multiplied by [a]'s {!kind_size_in_bytes}.

    @since 4.03.0 *)

  val get: ('a, 'b) t -> int -> 'a 
  (** [Array1.get a x], or alternatively [a.{x}],
     returns the element of [a] at index [x].
     [x] must be greater or equal than [0] and strictly less than
     [Array1.dim a] if [a] has C layout.  If [a] has Fortran layout,
     [x] must be greater or equal than [1] and less or equal than
     [Array1.dim a].  Otherwise, [Invalid_argument] is raised. *)

  val set: ('a, 'b) t -> int -> 'a -> unit 
  (** [Array1.set a x v], also written [a.{x} <- v],
     stores the value [v] at index [x] in [a].
     [x] must be inside the bounds of [a] as described in
     {!Bigarray.Array1.get};
     otherwise, [Invalid_argument] is raised. *)

  val sub: ('a, 'b) t -> int -> int -> ('a, 'b) t

  (** Extract a sub-array of the given one-dimensional big array.
     See {!Genarray.sub_left} for more details. *)


  val blit: ('a, 'b) t -> ('a, 'b) t -> unit  
  (** Copy the first big array to the second big array.
     See {!Genarray.blit} for more details. *)

  val fill: ('a, 'b) t -> 'a -> unit 
  (** Fill the given big array with the given value.
     See {!Genarray.fill} for more details. *)

  val of_array: ('a, 'b) kind ->  'a array -> ('a, 'b) t
  (** Build a one-dimensional big array initialized from the
     given array.  *)


  val unsafe_get: ('a, 'b) t -> int -> 'a 
  (** Like {!Bigarray.Array1.get}, but bounds checking is not always performed.
      Use with caution and only when the program logic guarantees that
      the access is within bounds. *)

  val unsafe_set: ('a, 'b) t -> int -> 'a -> unit
  (** Like {!Bigarray.Array1.set}, but bounds checking is not always performed.
      Use with caution and only when the program logic guarantees that
      the access is within bounds. *)

end


(** {1 Two-dimensional arrays} *)

(** Two-dimensional arrays. The [Array2] structure provides operations
   similar to those of {!Bigarray.Genarray}, but specialized to the
   case of two-dimensional arrays. *)
module Array2 :
  sig
  type ('a, 'b) t
  (** The type of two-dimensional big arrays whose elements have
     OCaml type ['a], representation kind ['b], and memory layout ['c]. *)

  val create: ('a, 'b) kind ->   int -> int -> ('a, 'b) t
  (** [Array2.create kind layout dim1 dim2] returns a new bigarray of
     two dimension, whose size is [dim1] in the first dimension
     and [dim2] in the second dimension.  [kind] and [layout]
     determine the array element kind and the array layout
     as described for {!Bigarray.Genarray.create}. *)

  val dim1: ('a, 'b) t -> int 
  (** Return the first dimension of the given two-dimensional big array. *)

  val dim2: ('a, 'b) t -> int 
  (** Return the second dimension of the given two-dimensional big array. *)

  val kind: ('a, 'b) t -> ('a, 'b) kind 
  (** Return the kind of the given big array. *)



  val size_in_bytes : ('a, 'b) t -> int
  (** [size_in_bytes a] is the number of elements in [a]
    multiplied by [a]'s {!kind_size_in_bytes}.

    @since 4.03.0 *)

  val get: ('a, 'b) t -> int -> int -> 'a 
  (** [Array2.get a x y], also written [a.{x,y}],
     returns the element of [a] at coordinates ([x], [y]).
     [x] and [y] must be within the bounds
     of [a], as described for {!Bigarray.Genarray.get};
     otherwise, [Invalid_argument] is raised. *)

  val set: ('a, 'b) t -> int -> int -> 'a -> unit 
  (** [Array2.set a x y v], or alternatively [a.{x,y} <- v],
     stores the value [v] at coordinates ([x], [y]) in [a].
     [x] and [y] must be within the bounds of [a],
     as described for {!Bigarray.Genarray.set};
     otherwise, [Invalid_argument] is raised. *)

  val sub_left: ('a, 'b) t -> int -> int -> ('a, 'b) t
  (** Extract a two-dimensional sub-array of the given two-dimensional
     big array by restricting the first dimension.
     See {!Bigarray.Genarray.sub_left} for more details.
     [Array2.sub_left] applies only to arrays with C layout. *)
  
  val slice_left: ('a, 'b) t -> int -> ('a, 'b) Array1.t
  (** Extract a row (one-dimensional slice) of the given two-dimensional
     big array.  The integer parameter is the index of the row to
     extract.  See {!Bigarray.Genarray.slice_left} for more details.
     [Array2.slice_left] applies only to arrays with C layout. *)

  
  val blit: ('a, 'b) t -> ('a, 'b) t -> unit    
  (** Copy the first big array to the second big array.
     See {!Bigarray.Genarray.blit} for more details. *)

  val fill: ('a, 'b) t -> 'a -> unit
  (** Fill the given big array with the given value.
     See {!Bigarray.Genarray.fill} for more details. *)

  val of_array: ('a, 'b) kind ->  'a array array -> ('a, 'b) t
  (** Build a two-dimensional big array initialized from the
     given array of arrays.  *)

  val unsafe_get: ('a, 'b) t -> int -> int -> 'a
  (** Like {!Bigarray.Array2.get}, but bounds checking is not always
      performed. *)

  val unsafe_set: ('a, 'b) t -> int -> int -> 'a -> unit                     
  (** Like {!Bigarray.Array2.set}, but bounds checking is not always
      performed. *)

end

(** {1 Three-dimensional arrays} *)

(** Three-dimensional arrays. The [Array3] structure provides operations
   similar to those of {!Bigarray.Genarray}, but specialized to the case
   of three-dimensional arrays. *)
module Array3 :
  sig
  type ('a, 'b) t
  (** The type of three-dimensional big arrays whose elements have
     OCaml type ['a], representation kind ['b], and memory layout ['c]. *)

  val create: ('a, 'b) kind ->  int -> int -> int -> ('a, 'b) t
  (** [Array3.create kind layout dim1 dim2 dim3] returns a new bigarray of
     three dimension, whose size is [dim1] in the first dimension,
     [dim2] in the second dimension, and [dim3] in the third.
     [kind] and [layout] determine the array element kind and
     the array layout as described for {!Bigarray.Genarray.create}. *)

  val dim1: ('a, 'b) t -> int 
  (** Return the first dimension of the given three-dimensional big array. *)

  val dim2: ('a, 'b) t -> int 
  (** Return the second dimension of the given three-dimensional big array. *)

  val dim3: ('a, 'b) t -> int 
  (** Return the third dimension of the given three-dimensional big array. *)

  val kind: ('a, 'b) t -> ('a, 'b) kind 
  (** Return the kind of the given big array. *)

  val size_in_bytes : ('a, 'b) t -> int
  (** [size_in_bytes a] is the number of elements in [a]
    multiplied by [a]'s {!kind_size_in_bytes}.

    @since 4.03.0 *)

  val get: ('a, 'b) t -> int -> int -> int -> 'a 
  (** [Array3.get a x y z], also written [a.{x,y,z}],
     returns the element of [a] at coordinates ([x], [y], [z]).
     [x], [y] and [z] must be within the bounds of [a],
     as described for {!Bigarray.Genarray.get};
     otherwise, [Invalid_argument] is raised. *)

  val set: ('a, 'b) t -> int -> int -> int -> 'a -> unit
  (** [Array3.set a x y v], or alternatively [a.{x,y,z} <- v],
     stores the value [v] at coordinates ([x], [y], [z]) in [a].
     [x], [y] and [z] must be within the bounds of [a],
     as described for {!Bigarray.Genarray.set};
     otherwise, [Invalid_argument] is raised. *)

  val sub_left: ('a, 'b) t -> int -> int -> ('a, 'b) t    
  (** Extract a three-dimensional sub-array of the given
     three-dimensional big array by restricting the first dimension.
     See {!Bigarray.Genarray.sub_left} for more details.  [Array3.sub_left]
     applies only to arrays with C layout. *)

  val slice_left_1:
    ('a, 'b) t -> int -> int -> ('a, 'b) Array1.t
  (** Extract a one-dimensional slice of the given three-dimensional
     big array by fixing the first two coordinates.
     The integer parameters are the coordinates of the slice to
     extract.  See {!Bigarray.Genarray.slice_left} for more details.
     [Array3.slice_left_1] applies only to arrays with C layout. *)

  val slice_left_2: ('a, 'b) t -> int -> ('a, 'b) Array2.t
  (** Extract a  two-dimensional slice of the given three-dimensional
     big array by fixing the first coordinate.
     The integer parameter is the first coordinate of the slice to
     extract.  See {!Bigarray.Genarray.slice_left} for more details.
     [Array3.slice_left_2] applies only to arrays with C layout. *)

  val blit: ('a, 'b) t -> ('a, 'b) t -> unit
  (** Copy the first big array to the second big array.
     See {!Bigarray.Genarray.blit} for more details. *)

  val fill: ('a, 'b) t -> 'a -> unit 
  (** Fill the given big array with the given value.
     See {!Bigarray.Genarray.fill} for more details. *)

  val of_array:
    ('a, 'b) kind ->  'a array array array -> ('a, 'b) t
  (** Build a three-dimensional big array initialized from the
     given array of arrays of arrays.  *)


  val unsafe_get: ('a, 'b) t -> int -> int -> int -> 'a
  (** Like {!Bigarray.Array3.get}, but bounds checking is not always
      performed. *)

  val unsafe_set: ('a, 'b) t -> int -> int -> int -> 'a -> unit
  (** Like {!Bigarray.Array3.set}, but bounds checking is not always
      performed. *)

end


external genarray_of_array1 :
  ('a, 'b) Array1.t -> ('a, 'b) Genarray.t = "%identity"
(** Return the generic big array corresponding to the given one-dimensional
   big array. *)

external genarray_of_array2 :
  ('a, 'b) Array2.t -> ('a, 'b) Genarray.t = "%identity"
(** Return the generic big array corresponding to the given two-dimensional
   big array. *)

external genarray_of_array3 :
  ('a, 'b) Array3.t -> ('a, 'b) Genarray.t = "%identity"
(** Return the generic big array corresponding to the given three-dimensional
   big array. *)


val array1_of_genarray : ('a, 'b) Genarray.t -> ('a, 'b) Array1.t
(** Return the one-dimensional big array corresponding to the given
   generic big array.  Raise [Invalid_argument] if the generic big array
   does not have exactly one dimension. *)

val array2_of_genarray : ('a, 'b) Genarray.t -> ('a, 'b) Array2.t
(** Return the two-dimensional big array corresponding to the given
   generic big array.  Raise [Invalid_argument] if the generic big array
   does not have exactly two dimensions. *)

val array3_of_genarray : ('a, 'b) Genarray.t -> ('a, 'b) Array3.t
(** Return the three-dimensional big array corresponding to the given
   generic big array.  Raise [Invalid_argument] if the generic big array
   does not have exactly three dimensions. *)


(** {1 Re-shaping big arrays} *)

val reshape : ('a, 'b) Genarray.t -> int array -> ('a, 'b) Genarray.t
(** [reshape b [|d1;...;dN|]] converts the big array [b] to a
   [N]-dimensional array of dimensions [d1]...[dN].  The returned
   array and the original array [b] share their data
   and have the same layout.  For instance, assuming that [b]
   is a one-dimensional array of dimension 12, [reshape b [|3;4|]]
   returns a two-dimensional array [b'] of dimensions 3 and 4.
   If [b] has C layout, the element [(x,y)] of [b'] corresponds
   to the element [x * 3 + y] of [b].  If [b] has Fortran layout,
   the element [(x,y)] of [b'] corresponds to the element
   [x + (y - 1) * 4] of [b].
   The returned big array must have exactly the same number of
   elements as the original big array [b].  That is, the product
   of the dimensions of [b] must be equal to [i1 * ... * iN].
   Otherwise, [Invalid_argument] is raised. *)

val reshape_1 : ('a, 'b) Genarray.t -> int -> ('a, 'b) Array1.t
(** Specialized version of {!Bigarray.reshape} for reshaping to
   one-dimensional arrays. *)

val reshape_2 : ('a, 'b) Genarray.t -> int -> int -> ('a, 'b) Array2.t
(** Specialized version of {!Bigarray.reshape} for reshaping to
   two-dimensional arrays. *)

val reshape_3 :
  ('a, 'b) Genarray.t -> int -> int -> int -> ('a, 'b) Array3.t
(** Specialized version of {!Bigarray.reshape} for reshaping to
   three-dimensional arrays. *)
