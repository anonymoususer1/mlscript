

module B = F32Binding

type elt = B.elt
let %private { 
  unsafe_get = (.![]);
  unsafe_set = (.![]<-)} = (module Array)

type t = {
  shape : TensorShape.t;
  data : B.t
  (* The js B.t keeps track of
     such properties which may be useful
     - BYTES_PER_ELEMENT
     - get buffer 
     - get byteLength // the whole size of the data
     - get byteOffset
     - get length // track the number of elements for you
  *)
}
(**
   Invariants:
   [size_of_shape shape] = length (data)
*)


(** return the size of elements *)
let size ({data} : t ) = B.length data
let shape (x : t ) = x.shape
let toArray (x : t) = 
  let size = size x in 
  let output = Array.make size 0. in 
  for i = 0  to size - 1 do 
    output.![i] <- B.unsafe_get x.data i   
  done ; 
  output  

let zeros (shape : TensorShape.t) = 
  let size = TensorShape.size shape in 
  let data = B.make size in 
  { shape ; data}

let dims (x : t) = x.shape |. TensorShape.dims

let get (x : t ) (ind : int array) : elt = 
  assert (Array.length ind = dims x );
  let ind = (TensorShape.calc_offset x.shape ind) in 
  B.unsafe_get x.data ind

let set (x : t ) (ind : int array) (v : elt) = 
  assert (Array.length ind = dims x );
  let ind = (TensorShape.calc_offset x.shape ind) in 
  B.unsafe_set x.data ind v 

(* output share the same dimension as input *)
let subarr ({shape} as x : t ) offset len : t  =
  let dim = TensorShape.dims shape in 
  assert (dim > 0);
  let first_1d = shape.![0] in 
  assert (offset >=0 && len > 0 && offset + len <= first_1d);
  let new_shape = TensorShape.replace1D shape len in 
  let s_size = TensorShape.size ~offs:1 shape in 
  let offset = s_size * offset in 
  let next = offset + s_size * len in 
  let new_view = x.data |. B.subarray ~beg:offset ~end_:next in  
  {shape = new_shape; data = new_view}  


(** Extract a sub-array of lower dimension from the given big array
     by fixing one or several of the first (left-most) coordinates.
     [Genarray.slice_left a [|i1; ... ; iM|]] returns the 'slice'
     of [a] obtained by setting the first [M] coordinates to
     [i1], ..., [iM].  If [a] has [N] dimensions, the slice has
     dimension [N - M], and the element at coordinates
     [[|j1; ...; j(N-M)|]] in the slice is identical to the element
     at coordinates [[|i1; ...; iM; j1; ...; j(N-M)|]] in the original
     array [a].  No copying of elements is involved: the slice and
     the original array share the same storage space.
*)
let slice ({shape} as x : t) (index : int array) : t = 
  let dim = Array.length shape in 
  let sub_len = Array.length index in 
  assert (sub_len > 0 && sub_len < dim);
  let offset = TensorShape.calc_offset  shape index in 
  (* invariant is checked in calc_offset *)
  (* for i = 0 to sub_len - 1 do 
     assert (shape.![i] > index.![i])  
     done;    *)
  let new_shape = Array.sub shape sub_len (dim -sub_len) in 
  let s_size = TensorShape.size new_shape in 

  let new_data = 
    x.data |. B.subarray ~beg:offset ~end_:(offset + s_size) in 
  { shape = new_shape ; data = new_data}  



(** Do the invariant check and return the shared view *)
let reshape (x : t) shape =
  let s = TensorShape.size shape in 
  assert (s = size x);
  {x with shape = shape}

(* let update ({data; shape} : t )  f = 
   for i = 0 to TensorShape.size shape - 1 do 
    B.unsafe_set data i 
   done    *)

let from ( data : elt array) : t = 
  {data = B.from data ; shape = TensorShape.d1 (Array.length data) }

let from2D (data : elt array array) : t = 
  let d0 = Array.length data in 
  if d0 = 0 then {data = B.make 0; shape = TensorShape.d2 0 0}  
  else 
    let d1 = Array.length data.![0] in   
    let same = data |. Belt.Array.every (fun x -> Array.length x = d1 ) in 
    if not same  then invalid_arg "not regular matrix"
    else 
      let size = d0 * d1 in 
      let newdata = B.make size in 
      let cursor = ref 0 in 
      for i = 0 to d0 - 1 do 
        for j = 0 to d1 - 1 do 
          B.unsafe_set newdata (!cursor + j ) data.![i].![j]
        done;
        cursor := !cursor + d1 ; 
      done;
      {shape = TensorShape.d2 d0 d1 ; data = newdata}     


let from3D (data : elt array array array) : t =
  let d0 = Array.length data in 
  if d0 = 0 then {data = B.make 0; shape = TensorShape.d3 0 0 0}  
  else 
    let d1 = Array.length data.![0] in   
    let same = data |. Belt.Array.every (fun x -> Array.length x = d1 ) in     
    if not same  then invalid_arg "not regular matrix"
    else 
    if d1 = 0 then {data = B.make 0; shape = TensorShape.d3 d0 0 0}
    else 
      let d2 = Array.length data.![0].![0] in 
      let same = 
        data 
        |. Belt.Array.every 
          (fun x ->
             x |. Belt.Array.every (fun x -> Array.length x = d2 )
          ) in 
      if not same then invalid_arg "not regular matrix"   
      else 
        let size = d0 * d1 * d2 in 
        let newdata = B.make size in 
        let d1d2 = d1 * d2 in 
        for i = 0 to d0 - 1 do 
          for j = 0 to d1 - 1 do 
            for k = 0 to d2 - 1 do   
              B.unsafe_set newdata (i * d1d2  + j * d2 + k  ) data.![i].![j].![k]
            done 
          done;
        done;
        {shape = TensorShape.d3 d0 d1 d2; data = newdata}     




let toIntArray (x : t) = 
  let size = size x in 
  let output = Array.make size 0 in 
  for i = 0  to size - 1 do 
    output.![i] <- int_of_float (B.unsafe_get x.data i)
  done ; 
  output  

let dummy  : t = {shape = [||]; data = B.make 0}
(**
  * check the similar semantics with `n.arange(1.2,10.1)`
  * The range is `[)`
  * step can not be zer0
  * [begin, begin + step , ... begin + n * step]
  * where `begin + n * step < end_`
  * `begin + n * step + 1 >= end_ `
  * https://numpy.org/doc/stable/reference/generated/numpy.arange.html
*)
let arange ?(step = 1) beg end_  = 
  assert (step <> 0);
  if end_ = beg then {data = B.make 0 ;shape = TensorShape.d1 0 }
  else 
    let size = (end_ - beg - 1) / step + 1 in 
    (* (size - 1) * step <= end - beg - 1 
       (size - 1) * step + beg <= end - 1

       (size - 1) * step > end - beg - 2
       size * step + beg > end - 2 + step 

       When step >=2 it is correct
       When step = 1 it is also correct
    *)
    let data = B.make size in 
    let cur = ref (float beg) in 
    for i = 0 to size - 1 do 
      B.unsafe_set data i !cur  ;
      cur := !cur +.  float step
    done ;
    { data ; shape = TensorShape.d1 size} 

;;

(** deep copy so that no data is shared 
    The freshly created array maybe more compact 
*)
let copy ({shape; data} ) =   
  {shape; data = B.slice data }

let copy_ ?out  ({shape; data} as from)  =
  match out with 
  | None -> copy from 
  | Some dst ->   
    assert (TensorShape.eq shape dst.shape) ; 
    let dst_data = dst.data in 
    for i = 0 to TensorShape.size shape - 1 do 
      B.unsafe_set dst_data i (B.unsafe_get data i)  
    done; 
    dst 

let forall  ({data} as x : t) (f : elt -> bool [@bs]) = 
  let yes = ref true  in 
  let i = ref 0 in 
  let s = size x in 
  while !yes && !i < s do 
    if f (B.unsafe_get data !i ) [@bs] then incr i 
    else yes := false 
  done;
  !yes
let no_nan x = forall x (fun[@bs] x -> classify_float x <> FP_nan) 
let sum_all ({data} as x : t) : elt  = 
  let result = ref B.zero  in 
  for i = 0 to size x - 1 do 
    result := !result +. B.unsafe_get data i   
  done;
  !result  

let exp (x : t) = { x with data = B.map x.data exp }  
let neg (x : t ) = { x with data = B.map x.data (~-.)}

let as_scalar (x  : t ) : elt = 
  assert (size x = 1);
  B.unsafe_get x.data 0 



let shuffle_dim0 ( {data = self} as x : t) : t = 
  let dim = dims x in 
  assert (dim > 0) ; 
  let dim0 = x.shape.![0] in 
  let v = Belt.Array.range 0 (dim0 - 1) in 
  v |. Belt.Array.shuffleInPlace;
  let size = size x in
  let data = B.make size in 
  let len = size / dim0 in
  let off = ref 0 in  
  for i = 0 to dim0 - 1 do 
    data 
    |. B.set 
      (B.subarray self ~beg:(len * v.![i]) ~end_:(len * v.![i] + len))
      ~offset:!off;
    off :=  !off + len;   
  done;
  { x with data = data} 



(* assume that 
   len <= x.length
   len <= y.length
*)  
let eqAux (x : B.t) (y : B.t) len = 
  let cursor = ref 0 in 
  let eq = ref true in 
  while !eq && !cursor < len do 
    let a, b = B.unsafe_get x !cursor, B.unsafe_get y !cursor in 
    if a <> b then eq := false    
    else incr cursor
  done;
  !eq

let [@inline] unopLinearAux (u : B.t) (len : int) (start : int) (step : int) op  = 
  let {unsafe_get = (.![]); unsafe_set = _ } = (module B) in   
  let value = ref u.![start] in 
  let offset = ref (start + step) in 
  for _ = 1 to len - 1 do 
    value := op !value u.![!offset] [@bs];
    offset := !offset + step
  done;
  !value   
;;

let [@inline] unopLinearWithInitAux (u : B.t) (init : elt) len start step op  = 
  let {unsafe_get = (.![]); unsafe_set = _ } = (module B) in   
  let value = ref init in 
  let offset = ref start in 
  for _ = 0 to len - 1 do  (* it is zero here *)
    value := op ~acc:!value u.![!offset] [@bs];
    offset := !offset + step
  done;
  !value   
;;

let [@inline] unopLinearIndexAux (u : B.t) len start step 
    (op : (elt -> elt -> bool [@bs]))  : int = 
  let {unsafe_get = (.![]); unsafe_set = _ } = (module B) in   
  let value = ref u.![start] in 
  let idx = ref 0 in 
  let offset = ref (start + step) in 
  for i = 1 to len - 1 do 
    let next =  u.![!offset] in 
    if op !value next [@bs] then begin 
      idx := i;
      value := next
    end;
    offset := !offset + step
  done;
  !idx
;;

(**TODO: could optimize *)
let reduce 
    ?(axis=(-1)) 
    ?(keepDims = true)
    ({shape} as x : t) (op : elt -> elt -> elt [@bs])  = 
  let dim = dims x in 
  let axis = 
    if axis < 0 then dim + axis else axis in       
  assert (axis >=0 && axis < dim);
  let stride = TensorShape.calc_stride x.shape in 
  let step = stride.![axis] in 
  let len = shape.![axis] in (* sample 1 from len *)
  let newsize = size x / len in 
  let newShape =
    if keepDims then TensorShape.replaceID shape axis 1
    else TensorShape.removeID shape axis
  in 
  let data = B.make newsize in 
  let count = ref 0 in 
  let start = ref 0 in 
  let larger_tride = (len - 1) * step  in 
  (** every step, we leap (len - 1) * step *)
  for i = 0 to newsize - 1 do       
    B.unsafe_set data i (unopLinearAux x.data len !start step op) ; 
    incr count;
    incr start; 
    if !count = step then begin 
      count := 0;  
      start := !start + larger_tride
    end 
  done  ;
  {data ; shape = newShape} 

let reduce_ ({data } as _result : t)
    ?(axis=(-1))     
    ({shape} as x : t) (op : elt -> elt -> elt [@bs])  = 
  let dim = dims x in 
  let axis = 
    if axis < 0 then dim + axis else axis in       
  assert (axis >=0 && axis < dim);
  let stride = TensorShape.calc_stride x.shape in 
  let step = stride.![axis] in 
  let len = shape.![axis] in (* sample 1 from len *)
  let newsize = size x / len in 
  (* let _newShape =
     if keepDims then TensorShape.replaceID shape axis 1
     else TensorShape.removeID shape axis
     in  *)
  (* let data = B.make newsize in  *)
  (** FIXME: invariant checking shape *)
  let count = ref 0 in 
  let start = ref 0 in 
  let larger_tride = (len - 1) * step  in 
  (** every step, we leap (len - 1) * step *)
  for i = 0 to newsize - 1 do       
    B.unsafe_set data i (unopLinearAux x.data len !start step op) ; 
    incr count;
    incr start; 
    if !count = step then begin 
      count := 0;  
      start := !start + larger_tride
    end 
  done 



let sampleUI8 
    ?(axis=(-1))  
    ?(keepDims = false)
    ({shape;data = x_data} as x : t) (op : UI8.t)  = 
  let dim = dims x in 
  let axis = 
    if axis < 0 then dim + axis else axis in       
  assert (axis >=0 && axis < dim);
  let stride = TensorShape.calc_stride x.shape in 
  let step = stride.![axis] in 
  let len = shape.![axis] in (* sample 1 from len *)
  let newsize = size x / len in 
  assert (UI8.dims op = 1 && UI8.size op = newsize);
  assert (UI8.forall op (fun [@bs] x -> x < len));

  let newShape = 
    if keepDims then TensorShape.replaceID shape axis 1 
    else TensorShape.removeID shape axis in 
  let data = B.make newsize in 
  let count = ref 0 in 
  let start = ref 0 in 
  let larger_tride = (len - 1) * step  in   

  (** every step, we leap (len - 1) * step *)
  for i = 0 to newsize - 1 do       
    let k = UI8.B.unsafe_get op.data i in 

    B.unsafe_set data i (B.unsafe_get x_data (!start + k * step )) ;     
    incr count;
    incr start; 
    if !count = step then begin 
      count := 0;  
      start := !start + larger_tride
    end 
  done  ;
  {data ; shape = newShape} 
;;


let fold ?(axis=(-1)) ?(init=B.zero) ({shape} as x : t) (op : acc:elt -> elt -> elt [@bs])  = 
  let dim = dims x in 
  let axis = 
    if axis < 0 then dim + axis else axis in       
  assert (axis >=0 && axis < dim);
  let stride = TensorShape.calc_stride x.shape in 
  let step = stride.![axis] in 
  let len = shape.![axis] in (* sample 1 from len *)
  let newsize = size x / len in 
  let newShape =TensorShape.replaceID shape axis 1 in 
  let data = B.make newsize in 
  let count = ref 0 in 
  let start = ref 0 in 
  let larger_tride = (len - 1) * step  in 
  (** every step, we leap (len - 1) * step *)
  for i = 0 to newsize - 1 do       
    B.unsafe_set data i (unopLinearWithInitAux x.data init len !start step op) ; 
    incr count;
    incr start; 
    if !count = step then begin 
      count := 0;  
      start := !start + larger_tride
    end 
  done  ;
  {data ; shape = newShape}   

let unopIndex ?(axis=(-1))  ({shape} as x : t) (op : elt -> elt -> bool [@bs])  = 
  let dim = dims x in 
  let axis = 
    if axis < 0 then dim + axis else axis in       
  assert (axis >=0 && axis < dim);
  let stride = TensorShape.calc_stride x.shape in 
  let step = stride.![axis] in 
  let len = shape.![axis] in (* sample 1 from len *)
  let newsize = size x / len in 
  let newShape =TensorShape.replaceID shape axis 1 in 
  let i32Tensor = I32.zeros newShape in 
  let count = ref 0 in 
  let start = ref 0 in 
  let larger_tride = (len - 1) * step  in 
  (** every step, we leap (len - 1) * step *)
  for i = 0 to newsize - 1 do       
    I32.B.unsafe_set i32Tensor.data i (unopLinearIndexAux x.data len !start step op) ; 
    incr count;
    incr start; 
    if !count = step then begin 
      count := 0;  
      start := !start + larger_tride
    end 
  done  ;
  i32Tensor


let max ?axis ?keepDims tensor = reduce ?axis ?keepDims tensor (fun[@bs] x y -> max x y)
let min ?axis ?keepDims tensor = reduce ?axis ?keepDims tensor (fun[@bs] x y -> min x y)
let sum ?axis ?keepDims tensor = reduce ?axis ?keepDims tensor (fun [@bs] x y -> x +. y)

let sum_ ?axis ?out tensor  = 
  match out with 
  | None -> 
    reduce ?axis  tensor (fun [@bs] x y -> x +. y) ~keepDims:false 
  | Some result -> 
    reduce_ ?axis result tensor (fun [@bs] x y -> x +. y);
    result 

let square_sum ?axis tensor = 
  fold ?axis tensor (fun [@bs] ~acc y -> acc +. y*.y)

let argmax ?axis tensor = 
  unopIndex ?axis tensor (fun [@bs] x y -> x < y)
let argmin ?axis tensor = 
  unopIndex ?axis tensor (fun [@bs] x y -> x > y)


let fill (x : t) (v : float) = 
  x.data |. B.fill v 

let eq (x : t) (y : t) = 
  TensorShape.eq x.shape y.shape &&
  eqAux x.data y.data (size x)

let neq x y = 
  not (eq x y)

(* in place map *)  
let [@inline] map_ ({data} as x : t) (f : float -> float [@bs]) = 
  for i = 0 to size x - 1 do 
    B.unsafe_set data i (f (B.unsafe_get data i) [@bs])
  done;
  x      


let [@inline] map ({data; shape} as x : t) (f : float -> float [@bs]) = 
  let {data= new_data} as new_tensor = zeros shape in    
  for i = 0 to size x - 1 do 
    B.unsafe_set new_data i (f (B.unsafe_get data i) [@bs])
  done ;
  new_tensor

(* This should be optimized as hand written *)  
let rand (shape : TensorShape.t) = 
  let data = zeros shape in 
  data |. map_ (fun [@bs] _ -> RandomUtil.rand () )
;;

let randn (shape : TensorShape.t) = 
  let data = zeros shape in 
  data |. map_ (fun [@bs] _ -> RandomUtil.randn ())


let to_dimension ( x : t ) dim = 
  { x with shape = TensorShape.to_dimension x.shape dim}  
(** if we always ask user to keep the dimension
    it would be simplified

*)  
(** a is of shape [2,2,3,3], b is of shape [1,1,3,3]
*)  
let a_op_b a b (op : elt -> elt -> elt [@bs]) =    
  let size_b = size b in 
  let b_cursor = ref 0 in 
  let {data } as  tensor = zeros (shape a ) in 
  let a_data = a.data in let b_data = b.data in 
  for i = 0 to size a - 1 do 
    B.unsafe_set data i 
      (op (B.unsafe_get a_data i ) (B.unsafe_get b_data !b_cursor) [@bs]);
    incr b_cursor;
    if !b_cursor = size_b then b_cursor := 0
  done;
  tensor  

let a_op_b_ ({data} as tensor ) a b (op : elt -> elt -> elt [@bs]) =    
  (* FIXME: invariant checking *)  
  let size_b = size b in 
  let b_cursor = ref 0 in 
  let a_data = a.data in let b_data = b.data in 
  for i = 0 to size a - 1 do 
    B.unsafe_set data i 
      (op (B.unsafe_get a_data i ) (B.unsafe_get b_data !b_cursor) [@bs]);
    incr b_cursor;
    if !b_cursor = size_b then b_cursor := 0
  done;
  tensor    
(** a is of shape [2,3,3,2], b is of shape [2,3,1,1]*)  
let leading_a_op_b a b (op : elt -> elt -> elt [@bs]) = 
  let size_a = size a in 
  let size_b = size b in 
  let size_c = size_a / size_b in 
  let { data} as tensor = zeros (shape a) in 
  let a_data = a.data in
  let b_data = b.data in 
  let counter = ref 0 in 
  let b_cursor = ref 0 in 
  for i = 0 to size_a - 1 do     
    B.unsafe_set data i 
      (op (B.unsafe_get a_data i) (B.unsafe_get b_data !b_cursor )[@bs]) ;
    incr counter ; 
    if !counter = size_c then begin 
      counter := 0 ;
      incr b_cursor   
    end   
  done; 
  tensor   
(** a is of shape [2,3,3,2] b is of shape [1,3,3,1] *)

let leading_a_op_b_ ({data} as tensor ) a b (op : elt -> elt -> elt [@bs]) = 
  let size_a = size a in 
  let size_b = size b in 
  let size_c = size_a / size_b in 
  let a_data = a.data in
  let b_data = b.data in 
  let counter = ref 0 in 
  let b_cursor = ref 0 in 
  for i = 0 to size_a - 1 do     
    B.unsafe_set data i 
      (op (B.unsafe_get a_data i) (B.unsafe_get b_data !b_cursor )[@bs]) ;
    incr counter ; 
    if !counter = size_c then begin 
      counter := 0 ;
      incr b_cursor   
    end   
  done; 
  tensor   

(** Applicator 
    lift (+) $@ a $@ b 
*)  
let
  [@inline]
  broadcast_op (a : t) ( b : t) (op : elt -> elt -> elt [@bs]) =
  match TensorShape.classify a.shape b.shape with 
  | `same s -> 
    let {data } as  tensor = zeros s in 
    let a_data = a.data in let b_data = b.data in   
    for i =  0 to size a - 1 do 
      B.unsafe_set data i (op (B.unsafe_get a_data i)  (B.unsafe_get b_data i)[@bs])  
    done ;
    tensor 
  | `a_has_b_suffix _ -> 
    a_op_b a b op 
  | `b_has_a_suffix _ ->
    a_op_b b a (fun [@bs] x y -> op y x [@bs])
  | `b_has_a_prefix _ ->      
    leading_a_op_b b a (fun [@bs] x y -> op y x [@bs])
  | `a_has_b_prefix _ ->   
    leading_a_op_b a b op
  | _ -> assert false   


let
  [@inline]
  broadcast_op_ ({data} as tensor : t ) (a : t) ( b : t) (op : elt -> elt -> elt [@bs]) =
  match TensorShape.classify a.shape b.shape with 
  | `same _s -> 
    (* FIXME: invariant checking *)
    let a_data = a.data in let b_data = b.data in   
    for i =  0 to size a - 1 do 
      B.unsafe_set data i (op (B.unsafe_get a_data i)  (B.unsafe_get b_data i)[@bs])  
    done ;
    tensor 
  | `a_has_b_suffix _ -> 
    a_op_b_ tensor  a b op 
  | `b_has_a_suffix _ ->
    a_op_b_ tensor b a (fun [@bs] x y -> op y x [@bs])
  | `b_has_a_prefix _ ->      
    leading_a_op_b_ tensor b a (fun [@bs] x y -> op y x [@bs])
  | `a_has_b_prefix _ ->   
    leading_a_op_b_ tensor a b op
  | _ -> assert false     

let [@inline] lift op  a b = 
  broadcast_op a b op 

let [@inline] lift_ o a b op   = 
  broadcast_op_ o a b op 

let add = lift (fun [@bs] a b -> a +. b )  
let sub = lift (fun [@bs] a b -> a -. b )  
let mul = lift (fun [@bs] a b -> a *. b )  
let div = lift (fun [@bs] a b -> a /. b )  

let mul_ ?out a b  = 
  match out with 
  | None -> mul a b   
  | Some o -> 
    lift_ o a b  (fun [@bs] a b -> a *. b )  

let add_ ?out a b  = 
  match out with 
  | None -> add a b   
  | Some o -> 
    lift_ o a b (fun [@bs] a b -> a +. b )  

let sub_ ?out a b = 
  match out with 
  | None -> sub a b 
  | Some o -> 
    lift_ o a b (fun [@bs] a b -> a -. b )

let div_ ?out a b = 
  match out with 
  | None -> div a b 
  | Some o -> 
    lift_ o a b (fun [@bs] a b -> a /. b)    
let update_with_derivative 
    ?(lr=0.1) 
    ?(check_shape=true) ({data=x_data} as x) ({data = deriv_data} as deriv) = 
  if check_shape then 
    assert (TensorShape.eq x.shape deriv.shape);
  let s = size x in 
  for i = 0 to s - 1 do 
    B.unsafe_set x_data i 
      ((B.unsafe_get x_data i) -. lr *. (B.unsafe_get deriv_data i))  
  done     
;;

let apply_mask ?(check_shape = true) ({data=x_data} as x) ({data = mask_data} as mask) =
  if check_shape then 
    assert (TensorShape.eq x.shape mask.shape);
  let s = size x in 
  for i = 0 to s - 1 do 
    let v = B.unsafe_get mask_data i  in   
    if v <= 0. then 
      B.unsafe_set x_data i 0.    
  done     
(** This can be done since B.t maintains the view properly 
*)  
let sigmoid ( x : t) = 
  { x with data = B.map x.data (fun x -> 1. /. (1. +. Pervasives.exp (-. x )))}
let log ?(offset = 1e-7) (x : t) = {x with data = B.map x.data (fun x -> log (x +. offset))}

let softmax ( x : t) : t = 
  let x =  sub x  (max x) in 
  div (exp x) (sum(exp(x)))


(* https://numpy.org/doc/stable/reference/generated/numpy.isclose.html *)
let iscloseBy 
    ({data = x_data} as x : t) 
    ({data = y_data } as y : t) 
    (op : elt -> elt -> bool [@bs])= 
  let size_x = size x in 
  assert (size_x = size y);
  let good = ref true in 
  let i = ref 0 in 
  while  !good && !i < size_x do 
    if not (op (B.unsafe_get x_data  !i) (B.unsafe_get y_data !i) [@bs]) then
      good := false
    else incr i 
  done;
  !good   

let is_close ?(diff=1e-8) x y = 
  iscloseBy x y (fun [@bs] x y -> abs_float (x -. y) < diff)

let sum_aux 
    (t : B.t) 
    (offa : int) 
    (h : B.t) 
    (offb : int) 
    (strideb : int)  
    (len : int) 
  : elt = 
  let result = ref 0. in   
  let cursora = ref offa in  
  let cursorb = ref offb in 
  for _ = 0 to len - 1 do 
    result := !result +. B.unsafe_get t !cursora *. B.unsafe_get h !cursorb ;
    incr cursora; 
    cursorb := !cursorb + strideb
  done;
  !result 

(** Two dimensional matrix multiplication *)  
let mm result_data x_data y_data stride_a stride_b len= 
  let offa = ref 0 in 
  let offb = ref 0 in 
  let cursor = ref 0 in 
  for _ = 0 to stride_a - 1 do 
    for _ = 0 to   stride_b - 1 do 
      B.unsafe_set result_data !cursor (sum_aux x_data !offa y_data !offb stride_b len);
      incr cursor;
      incr offb;   
    done ;
    offb := 0;
    offa := !offa + len ;
    (* cursor := !cursor + strideb  *)
  done    


let mm_add result_data x_data y_data stride_a stride_b len offset = 
  let offa = ref 0 in 
  let offb = ref 0 in 
  let cursor = ref 0 in 
  for _ = 0 to stride_a - 1 do     
    for i = 0 to   stride_b - 1 do 
      let offs = B.unsafe_get offset i in   
      B.unsafe_set result_data !cursor 
        (sum_aux x_data !offa y_data !offb stride_b len +. offs);
      incr cursor;
      incr offb;   
    done ;
    offb := 0;
    offa := !offa + len ;
    (* cursor := !cursor + strideb  *)
  done    
#if 1
external mm : B.t -> B.t -> B.t -> int -> int -> int -> unit = "float32_mm" [@@val]
external mm_add : B.t -> B.t -> B.t -> int -> int -> int -> B.t -> unit = "float32_mm_add" [@@val]  
#end
(* https://numpy.org/doc/stable/reference/generated/numpy.dot.html  *)
let dot 
    ({data = x_data;shape = x_shape} as x : t ) 
    ({data = y_data; shape = y_shape} as y : t) : t = 
  let dims_x = dims x in 
  let dims_y = dims y in 
  match dims_x, dims_y with 
  | 1,1 ->
    let size_x = size x in 
    assert (size_x =  size y);
    let scalar = ref 0. in 
    for i = 0 to size_x - 1 do 
      scalar  := !scalar +. B.unsafe_get x_data i *. B.unsafe_get y_data i
    done ;
    from [|!scalar|]
  | 2, 2 -> 
    (* [[1,2,3],
        [4,5,6]
       ] * 
       [
         [2],
         [4],
         [5]
       ]

       (2,3) ~ (3,1)      
    *)
    let len = x_shape.![1] in 
    assert (len = y_shape.![0]);
    let {data  =result_data } as result = zeros [|x_shape.![0]; y_shape.![1]|] in 
    let stridea, strideb = x_shape.![0], y_shape.![1] in 
    mm result_data x_data y_data stridea strideb len;    
    result   

  | _, 1  -> 
    let y_len = y_shape.![0] in 
    assert (dims_x >= 2 && x_shape.![dims_x - 1] = y_len);
    let new_size = size x / y_len in 
    let {data= o_data } as output = zeros (TensorShape.replaceID x_shape (dims_x - 1) 1) in 
    let cursor = ref 0 in 
    let offa = ref 0 in 
    for _ = 0 to new_size - 1 do 
      B.unsafe_set o_data !cursor (sum_aux x_data !offa y_data 0 1 y_len);
      incr cursor;
      offa := !offa + y_len
    done;  
    output

  | _, _  -> assert false 


;;


let dot_ ?out 
    ({data = x_data;shape = x_shape} as x : t ) 
    ({data = y_data; shape = y_shape} as y : t) : t = 
  let dims_x = dims x in 
  let dims_y = dims y in 
  match dims_x, dims_y with 
  | 1,1 ->
    let size_x = size x in 
    assert (size_x =  size y);
    let scalar = ref 0. in 
    for i = 0 to size_x - 1 do 
      scalar  := !scalar +. B.unsafe_get x_data i *. B.unsafe_get y_data i
    done ;
    copy_ ?out (from [|!scalar|]) (* optimize *)
  | 2, 2 -> 
    (* [[1,2,3],
        [4,5,6]
       ] * 
       [
         [2],
         [4],
         [5]
       ]

       (2,3) ~ (3,1)      
    *)
    let len = x_shape.![1] in 
    assert (len = y_shape.![0]);
    begin match out with 
      | None -> 
        let {data  =result_data } as result = zeros [|x_shape.![0]; y_shape.![1]|] in 
        let stridea, strideb = x_shape.![0], y_shape.![1] in 
        mm result_data x_data y_data stridea strideb len;    
        result
      | Some ({data  =result_data } as result) -> 
        let stridea, strideb = x_shape.![0], y_shape.![1] in 
        mm result_data x_data y_data stridea strideb len;    
        result
    end    

  | _, 1  -> 
    let y_len = y_shape.![0] in 
    assert (dims_x >= 2 && x_shape.![dims_x - 1] = y_len);
    let new_size = size x / y_len in 
    begin match out with 
      | None ->
        let {data= o_data } as output = zeros (TensorShape.replaceID x_shape (dims_x - 1) 1) in 
        let cursor = ref 0 in 
        let offa = ref 0 in 
        for _ = 0 to new_size - 1 do 
          B.unsafe_set o_data !cursor (sum_aux x_data !offa y_data 0 1 y_len);
          incr cursor;
          offa := !offa + y_len
        done;  
        output
      | Some  ({data= o_data } as output) -> 
        let cursor = ref 0 in 
        let offa = ref 0 in 
        for _ = 0 to new_size - 1 do 
          B.unsafe_set o_data !cursor (sum_aux x_data !offa y_data 0 1 y_len);
          incr cursor;
          offa := !offa + y_len
        done;  
        output
    end 
  | _, _  -> assert false 
;;
let multiply_add ({shape = x_shape; data = x_data} as x : t) 
    ({shape = y_shape; data = y_data} as y : t) (b : t)  = 
  let dims_x, dims_w, dims_b = dims x, dims y , dims b in 
  match dims_x, dims_w, dims_b with 
  | 2, 2, 1 ->     
    (*FIXME: more invariant checking *)
    let len = x_shape.![1] in 
    assert (len = y_shape.![0]);    
    assert (y_shape.![1] = b.shape.![0]);
    let {data  =result_data } as result = zeros [|x_shape.![0]; y_shape.![1]|] in 
    let stridea, strideb = x_shape.![0], y_shape.![1] in 
    mm_add result_data x_data y_data stridea strideb len b.data;    
    result

  | _ -> 
    add (dot x y ) b 

let multiply_add_ ?out (*{data = result_data} as result*)
    ({shape = x_shape; data = x_data} as x : t) 
    ({shape = y_shape; data = y_data} as y : t) (b : t)  = 
  let dims_x, dims_w, dims_b = dims x, dims y , dims b in 
  match dims_x, dims_w, dims_b with 
  | 2, 2, 1 ->     
    (*FIXME: more invariant checking *)
    let len = x_shape.![1] in 
    assert (len = y_shape.![0]);    
    assert (y_shape.![1] = b.shape.![0]);
    let stridea, strideb = x_shape.![0], y_shape.![1] in 
    begin match out with 
      | None -> 
        let {data  =result_data } as result = zeros [|x_shape.![0]; y_shape.![1]|] in 
        mm_add result_data x_data y_data stridea strideb len b.data;
        result 
      | Some ({data = result_data} as result)  -> 
        mm_add result_data x_data y_data stridea strideb len b.data;
        result 
    end 
  | _ -> 
    add_ ?out (dot x y ) b 


let ofUI8 ({data = x_data } as x : UI8.t) : t = 
  let s = UI8.size x in 
  let data = B.make s in 
  for i = 0 to s - 1 do 
    B.unsafe_set data i (float (UI8.B.unsafe_get x_data i))
  done;
  { shape = x.shape; data }     
(**
   even `-` 
   cause an allocation
   -np.sum(np.log(y[np.arange(batch_size), t] + 1e-7))/batch_size
   shape checking..
*)
let cross_entropy_error 
    (y : t) (t : UI8.t) = 
  let batch_size = y.shape.(0) in 
  let y = (
    sampleUI8 y t 
    |. map (fun [@bs] x -> Pervasives.log (x +. 1e-7 ))
  ) in 
  -. (sum y |. as_scalar ) /. float batch_size  

let transpose ({data} as x : t) : t = 
  (* TODO: support ar *)    
  match x.shape with 
  |[|i ; j|] -> 
    let size = i * j in 
    let new_data = B.make size in 
    for k = 0 to j - 1 do 
      for l =  0 to i - 1 do 
        B.unsafe_set new_data 
          (k * i + l )  (B.unsafe_get data (l * j + k))
      done    
    done;
    {shape = [|j;i|]; data = new_data}   
  | _ -> assert false 


let (@) = dot 
let (+) = add 
let (-) = sub 
let ( * ) = mul 
let (/) = div     
