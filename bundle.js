//@ts-check
var fs = require("fs");

var esbuild = require("esbuild");
const { readdirSync } = fs;

var path = require("path");

var src = path.join(__dirname, "src");

var targets = ["src/F64layers.js"];

esbuild.build({
  entryPoints: targets,
  bundle: true,
  minifySyntax: true,
  sourcemap: false,
  target: "es2015",
  outdir: "src",
  outExtension: {
    ".js": ".bundle.js",
  },
  watch : true,
  // plugins: [plugin],
  format: "esm",
});
